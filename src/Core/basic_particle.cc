/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
     \brief Implements manipulation and analysis routines.
*/

#include "Core/basic_particle.h"

using namespace std;

meta_particle::meta_particle()
{
    species = pcat::NewStar;
    r[0] = 0;
    r[1] = 0;
    r[2] = 0;
    v[0] = 0;
    v[1] = 0;
    v[2] = 0;
    a[0] = 0;
    a[1] = 0;
    a[2] = 0;
    m = 0;
    id = 0;
    u = 0;
    rho = 0;
    ne = 0;
    nh = 0;
    hsml = 0;
    eps = 0;
    sfr = 0;
    age = 0;
    metz = 0;
    pot = 0;
} // meta_particle::meta_particle()


meta_particle::meta_particle(const meta_particle& s)
{
    species = s.species;
    r[0] = s.r[0];
    r[1] = s.r[1];
    r[2] = s.r[2];
    v[0] = s.v[0];
    v[1] = s.v[1];
    v[2] = s.v[2];
    a[0] = s.a[0];
    a[1] = s.a[1];
    a[2] = s.a[2];
    m    = s.m;
    id   = s.id;
    u    = s.u;
    rho  = s.rho;
    ne   = s.ne;
    nh   = s.nh;
    hsml = s.hsml;
    eps  = s.eps;
    sfr  = s.sfr;
    age  = s.age;
    metz = s.metz;
    pot  = s.pot;
} // meta_particle::meta_particle(const meta_particle& s)


void meta_particle::from(const massive_particle& s)
{
    species = s.species;
    r[0] = s.r[0];
    r[1] = s.r[1];
    r[2] = s.r[2];
    v[0] = s.v[0];
    v[1] = s.v[1];
    v[2] = s.v[2];
    a[0] = s.a[0];
    a[1] = s.a[1];
    a[2] = s.a[2];
    m    = s.m;
    id   = s.id;
    u    = 0;
    rho  = 0;
    ne   = 0;
    nh   = 0;
    hsml = 0;
    eps  = 0;
    sfr  = 0;
    age  = 0;
    metz = 0;
    pot  = s.pot;
} //  meta_particle::from(const massive_particle& s)


void meta_particle::from(const stellar_particle& s)
{
    species = pcat::NewStar;
    r[0] = s.r[0];
    r[1] = s.r[1];
    r[2] = s.r[2];
    v[0] = s.v[0];
    v[1] = s.v[1];
    v[2] = s.v[2];
    a[0] = s.a[0];
    a[1] = s.a[1];
    a[2] = s.a[2];
    m    = s.m;
    id   = s.id;
    u    = 0;
    rho  = 0;
    ne   = 0;
    nh   = 0;
    hsml = 0;
    eps  = 0;
    sfr  = 0;
    age  = s.age;
    metz = s.metz;
    pot  = s.pot;
} // meta_particle::from(const stellar_particle& s)


void meta_particle::from(const sph_particle& s)
{
    species = pcat::Gas;
    r[0] = s.r[0];
    r[1] = s.r[1];
    r[2] = s.r[2];
    v[0] = s.v[0];
    v[1] = s.v[1];
    v[2] = s.v[2];
    a[0] = s.a[0];
    a[1] = s.a[1];
    a[2] = s.a[2];
    m    = s.m;
    id   = s.id;
    u    = s.u;
    rho  = s.rho;
    ne   = s.ne;
    nh   = s.nh;
    hsml = s.hsml;
    sfr  = s.sfr;
    age  = 0;
    metz = s.metz;
    pot  = s.pot;
} // meta_particle::from(const sph_particle& s)


void meta_particle::to(massive_particle& s)
{
    s.species = species;
    s.r[0] = r[0];
    s.r[1] = r[1];
    s.r[2] = r[2];
    s.v[0] = v[0];
    s.v[1] = v[1];
    s.v[2] = v[2];
    s.a[0] = a[0];
    s.a[1] = a[1];
    s.a[2] = a[2];
    s.m    = m;
    s.id   = id;
    s.pot  = pot;
} // meta_particle::to(massive_particle& s)


void meta_particle::to(stellar_particle& s)
{
    s.r[0] = r[0];
    s.r[1] = r[1];
    s.r[2] = r[2];
    s.v[0] = v[0];
    s.v[1] = v[1];
    s.v[2] = v[2];
    s.a[0] = a[0];
    s.a[1] = a[1];
    s.a[2] = a[2];
    s.m    = m;
    s.id   = id;
    s.age  = age;
    s.metz = metz;
    s.pot  = pot;
} // meta_particle::to(stellar_particle& s)


void meta_particle::to(sph_particle& s)
{
    s.r[0] = r[0];
    s.r[1] = r[1];
    s.r[2] = r[2];
    s.v[0] = v[0];
    s.v[1] = v[1];
    s.v[2] = v[2];
    s.a[0] = a[0];
    s.a[1] = a[1];
    s.a[2] = a[2];
    s.m    = m;
    s.id   = id;
    s.u    = u;
    s.rho  = rho;
    s.ne   = ne;
    s.nh   = nh;
    s.hsml = hsml;
    s.sfr  = sfr;
    s.metz = metz;
    s.pot  = pot;
} //  meta_particle::to(sph_particle& s)


massive_particle::massive_particle()
{
    species = pcat::DarkMatter;
    r[0] = 0;
    r[1] = 0;
    r[2] = 0;
    v[0] = 0;
    v[1] = 0;
    v[2] = 0;
    a[0] = 0;
    a[1] = 0;
    a[2] = 0;
    m    = 0;
    id   = 0;
    pot  = 0;
} // massive_particle::massive_particle()


massive_particle::massive_particle(const massive_particle& s)
{
    species = s.species;
    r[0] = s.r[0];
    r[1] = s.r[1];
    r[2] = s.r[2];
    v[0] = s.v[0];
    v[1] = s.v[1];
    v[2] = s.v[2];
    a[0] = s.a[0];
    a[1] = s.a[1];
    a[2] = s.a[2];
    m    = s.m;
    id   = s.id;
    pot  = s.pot;
} // massive_particle::massive_particle(const massive_particle& s)


stellar_particle::stellar_particle()
{
    r[0] = 0;
    r[1] = 0;
    r[2] = 0;
    v[0] = 0;
    v[1] = 0;
    v[2] = 0;
    a[0] = 0;
    a[1] = 0;
    a[2] = 0;
    m    = 0;
    id   = 0;
    age  = 0;
    metz = 0;
    pot  = 0;
} // stellar_particle::stellar_particle()


stellar_particle::stellar_particle(const stellar_particle& s)
{
    r[0] = s.r[0];
    r[1] = s.r[1];
    r[2] = s.r[2];
    v[0] = s.v[0];
    v[1] = s.v[1];
    v[2] = s.v[2];
    a[0] = s.a[0];
    a[1] = s.a[1];
    a[2] = s.a[2];
    m    = s.m;
    id   = s.id;
    age  = s.age;
    metz = s.metz;
    pot  = s.pot;
} // stellar_particle::stellar_particle(const stellar_particle& s)


stellar_particle::stellar_particle(const massive_particle& s)
{
    r[0] = s.r[0];
    r[1] = s.r[1];
    r[2] = s.r[2];
    v[0] = s.v[0];
    v[1] = s.v[1];
    v[2] = s.v[2];
    a[0] = s.a[0];
    a[1] = s.a[1];
    a[2] = s.a[2];
    m    = s.m;
    id   = s.id;
    age  = 0;
    metz = 0;
    pot  = s.pot;
} // stellar_particle::stellar_particle(const massive_particle& s)


stellar_particle::stellar_particle(const sph_particle& s)
{
    r[0] = s.r[0];
    r[1] = s.r[1];
    r[2] = s.r[2];
    v[0] = s.v[0];
    v[1] = s.v[1];
    v[2] = s.v[2];
    a[0] = s.a[0];
    a[1] = s.a[1];
    a[2] = s.a[2];
    m    = s.m;
    id   = s.id;
    age  = 0;
    metz = s.metz;
    pot  = s.pot;
} // stellar_particle::stellar_particle(const sph_particle& s)


compact_sph::compact_sph()
{
    r[0] = 0;
    r[1] = 0;
    r[2] = 0;
    m    = 0;
    met  = 0;
    u    = 0;
} // compact_sph::compact_sph()


compact_sph::compact_sph(const compact_sph& s)
{
    r[0] = s.r[0];
    r[1] = s.r[1];
    r[2] = s.r[2];
    m    = s.m;
    met  = s.met;
    u    = s.u;
} // stellar_particle::stellar_particle(const sph_particle& s)


compact_sph compact_sph::operator= (const compact_sph& s)
{
    r[0] = s.r[0];
    r[1] = s.r[1];
    r[2] = s.r[2];
    m    = s.m;
    met  = s.met;
    u    = s.u;
    return *this;
} // compact_sph compact_sph::operator= (const compact_sph& s)


color_particle::color_particle()
{
    r[0] = 0;
    r[1] = 0;
    r[2] = 0;
    blue = 0;
    red  = 0;
}// color_particle::color_particle()


color_particle color_particle::operator= (const color_particle& s)
{
    r[0] = s.r[0];
    r[1] = s.r[1];
    r[2] = s.r[2];
    blue = s.blue;
    red  = s.red;
    return *this;
} // color_particle color_particle::operator= (const color_particle& s)


color_particle::color_particle(const color_particle& s)
{
    r[0] = s.r[0];
    r[1] = s.r[1];
    r[2] = s.r[2];
    blue = s.blue;
    red  = s.red;
} // color_particle::color_particle(const color_particle& s)
