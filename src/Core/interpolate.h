/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
    \brief Provides snapshot interpolation functionality.

    This is used to interpolate between two snapshots using a 3rd order accurate
    interpolation scheme described here:

             http://www.idius.net/and-now-a-two-point-cubic-spline/

    \todo Transform this into a class.

*******************************************************************************/

#ifndef INTERPOLATE_H
#define INTERPOLATE_H

#include "Core/global.h"
#include "Core/particles.h"
#include "Viz/image.h"


/*! Interpolates between the snapshots named file0 and file1, where file0 is the
    older of the two snapshots. This creates n_intermediate snapshots evenly
    spaced in time.*/
void interpolate(char* file0,
                 char* file1,
                 char* interp_file,
                 uint n_intermediate,
                 image* config,
                 bool only_images = false);

/*! Transfers particle data of particle type type_id from the snapshot named
    gal to the array of particle objects, named parts. Then sort the parts
    array by particle ID number. */
void snapshot_to_particles(meta_particle* parts, gadget_io* gal, uint type_id);

/*! Transfers particle data of particle type type_id from the array of
    particle objects named parts to the snapshot object named gal. */
void particles_to_snapshot(meta_particle* parts, gadget_io* gal, uint type_id);

/*! A comparison function for std::sort() to sort an array of particle objects
     by particle ID number. */
bool compare_idx(meta_particle a, meta_particle b);

class QString;

void save_image(gadget_io* g, QString imagename, image* config);


#endif
