/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
 *  \brief Contains the particle_type class for manipulating one category of
 *  particle. */

#ifndef PARTICLE_TYPE_H
#define PARTICLE_TYPE_H

#include "Core/global.h"
#include "IO/io.h"
#include "Core/basic_particle.h"


/*! \brief Performs computations and manipulations for one type  (category) of
     particles.

    A class for handling each type of particle separately. This is useful if we
    need to manipulate or analyze all gas particles or all disk particles, but
    we are uninterested in the other particle types present in the snapshot. */
class particle_type
{
protected:

    pcat              type_id;
    std::vector<meta_particle>    snap;
    gadget_io*         gal {nullptr};
    float             mass_in_slit {0};
    uint              number_in_slit {0};
    uint              n_snap {0};
    rad               theta {0};
    rad               phi {0};
    rad               alpha {0};
    kpc               width {0};
    kpc               length {0};
    bool              initialized {false};

public:

    /*! The parameterized constructor for immediate initialization. */
    particle_type(pcat type_id, gadget_io* _gal);

    /*! The void constructor for deferred initialization. */
    particle_type();

    ~particle_type() { free(); }

    /*! Performs initialization tasks shared by both constructors ( i.e.,
        operations that are not performed immediately by the
        deferred-initialization constructor */
    void init(pcat type_id, gadget_io* _gal);

    /*! Performs initialization tasks shared by both constructors ( i.e.,
        operations that are not performed immediately by the
        deferred-initialization constructor */
    void init(uint tid, gadget_io* _gal);

    /*! Free memory allocated by object of class particle_type. */
    void free()
    {
        if (initialized)
        {
            snap.clear();
            initialized = false;
        }
    }

    /*! Copy particle data from galaxy_data structure to snap array. */
    void populate_snap();

    /*! Copy particle information from snap back to galaxy_data structure
        i.e., update the structure named gal. */
    void update_gal();

    /*! Add all current particle information to the snapshot. */
    void renew_snapshot();

    /*! compute the center of mass of the current type of particles. */
    void compute_cm(float* cm);

    /*! Get the coordinates of the black hole with index which_bh. */
    bool find_bh(const uint which_bh, float* cm);

    /*! Set the mass of the nth particle to m. */
    void set_particle_mass(const uint n, const float m);

    /*! Set the metalicity of the nth particle to z. */
    void set_particle_metalicity(const uint n, const float z);

    /*! Set the creation time of the nth particle to timestamp.*/
    void set_particle_time(uint n, const float timestamp);

    /*! shift the position and velocity vectors by a specified amount. */
    void shift(const float* cm);

    /*! rotate the particle positions and velocities such that the new z-axis
        points in the direction of the spherical coordinate theta, phi. */
    void rotate(const rad _theta, float _phi);

    /*! set slit dimensions and orientation (position angle, alpha) */
    void set_slit(const kpc _width,
                  const kpc _length,
                  const rad _alpha = 0)
    {
        width = _width;
        length = _length;
        alpha = _alpha;
    }

    /*! Find the mass-weighted line-of-sight velocities of particles within
        the slit. This is the most time-consuming step in the procedure.
        Optimizations should focus on this */
    uint v_los(const uint first, float* v_los_array);

    float get_slit_mass() const
    {
        return mass_in_slit;
    }

    uint get_number_in_slit() const
    {
        return number_in_slit;
    }

    uint get_n_snap() const
    {
        return n_snap;
    }

    meta_particle* get_snap_pointer()
    {
        return snap.data();
    }

    void push_back(meta_particle p)
    {
        snap.push_back(p);
        n_snap = snap.size();
    }

    void pop_back()
    {
        snap.pop_back();
        n_snap = snap.size();
    }
};

#endif // PARTICLE_TYPE_H
