/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file

    \brief function definitions for interpolating between snapshots.

    \todo Do something about the redundant code here, i.e., the methods
     particles_to_snapshot() and snapshot_to_particles(). The code should be
     redesigned at a high level so that the code is only written in one place.
     The resulting code should be valid for any generic snapshot rather than
     only GADGET snapshots. For instance, the particle_type_group could have
     an append_to_snapshot(generic_snapshot & snap) member and / or a
     to_snapshot(generic_snapshot & snap).

 ******************************************************************************/

#include "IO/io.h"
#include "interpolate.h"
#include "Core/particles.h"

#include <algorithm>
#include <QString>
#include <QByteArray>

using namespace std;

void interpolate(char* file0,
                 char* file1,
                 char* interp_file,
                 uint n_intermediate,
                 image* config,
                 bool only_images)
{
    // read the snapshot files; create snapshot objects.

    gadget_io gal0(file0);
    gadget_io gal1(file1);

    // allocate a particle array for storing data from gal0.




    meta_particle* parts0 = new meta_particle [gal0.Ntot];

    // allocate a particle array for storing data from gal1.

    meta_particle* parts1 = new meta_particle [gal1.Ntot];

    // allocate a particle array for storing interpolated data.

    meta_particle* partsi = new meta_particle [gal0.Ntot];

    // the QString f is the name of the interpolated snapshot file

    QString f(interp_file);

    f = f + ".";

    // now compute the time interval and related quantities needed for
    // performing the interpolation.

    const float t_start = gal0.header.time;

    const float t_end = gal1.header.time;

    const float dt = 1.0 / (t_end - t_start);

    float t = t_start;

    float dt_interm = (t_end - t_start) / float((n_intermediate + 1));

    // transfer relevant data from the snapshot objects to the particle arrays.

    for (uint type = 0; type < 6; ++type)
    {
        snapshot_to_particles(parts0, &gal0, type);
        snapshot_to_particles(parts1, &gal1, type);
    }

    // perform the interpolation to create n_intermediate frames.

    for (uint interm = 0; interm < n_intermediate; ++interm)
    {
        t += dt_interm;

        const float t_0 = t_start - t;

        const float t_1 = t_end - t;

        const float ts = t_0 * t_1 * dt;

        const float frac = t_0 / t_1; // note: t_1 == 0 when t_end == t

        const float p = frac * frac;

        const float pp1 = 1.0 / (p + 1.0);

        const float T = -t_0 * dt;

        // Interpolate each type of particle separately
        for (uint type = 0; type < 6; ++type)
        {
            uint n_min0 = 0, n_min1 = 0;
            uint n_snap0 = gal0.header.npart[type];
            uint n_snap1 = gal1.header.npart[type];

            if (type > 0)
            {
                for (uint i = 0; i < type; ++i)
                {
                    n_min0 += gal0.header.npart[i];
                    n_min1 += gal1.header.npart[i];
                }
            }

            const uint max_idx0 = n_min0 + n_snap0;

            const uint max_idx1 = n_min1 + n_snap1;

            for (uint i = n_min0, j = n_min1; i < max_idx0;)
            {
                // only perform an interpolation if the particle IDs match
                if (parts0[i].id == parts1[j].id)
                {
                    // x-component
                    partsi[i].r[0] = pp1 * (parts0[i].r[0]
                                            + p * parts1[j].r[0]
                                            + (p * parts1[j].v[0]
                                               - parts0[i].v[0]
                                              ) * ts
                                           );

                    // y-component
                    partsi[i].r[1] = pp1 * (parts0[i].r[1]
                                            + p * parts1[j].r[1]
                                            + (p * parts1[j].v[1]
                                               - parts0[i].v[1]
                                              ) * ts
                                           );

                    // z-component
                    partsi[i].r[2] = pp1 * (parts0[i].r[2]
                                            + p * parts1[j].r[2]
                                            + (p * parts1[j].v[2]
                                               - parts0[i].v[2]
                                              ) * ts
                                           );

                    // velocities and other quantities could be linearly
                    // interpolated here, if needed.

                    partsi[i].v[0] = parts0[i].v[0];
                    partsi[i].v[1] = parts0[i].v[1];
                    partsi[i].v[2] = parts0[i].v[2];
                    partsi[i].m = parts0[i].m;
                    partsi[i].id = parts0[i].id;

                    partsi[i].u = parts0[i].u + (parts1[j].u - parts0[i].u) * T;

                    partsi[i].rho = parts0[i].rho;
                    partsi[i].ne = parts0[i].ne;
                    partsi[i].nh = parts0[i].nh;
                    partsi[i].hsml = parts0[i].hsml;
                    partsi[i].sfr = parts0[i].sfr;
                    partsi[i].age = parts0[i].age;
                    partsi[i].metz = parts0[i].metz;
                    partsi[i].pot = parts0[i].pot;

                    ++i;
                    ++j;
                }
                else if ((parts0[i].id > parts1[j].id) && j < (max_idx1 - 1))
                {
                    ++j;
                }
                else if (parts0[i].id < parts1[j].id)
                {
                    // The particle no longer exists. Set properties to zero.
                    partsi[i].r[0] = 1e14;
                    partsi[i].r[1] = 1e14;
                    partsi[i].r[2] = 1e14;
                    partsi[i].v[0] = 0;
                    partsi[i].v[1] = 0;
                    partsi[i].v[2] = 0;
                    partsi[i].m = 0;
                    partsi[i].id = parts0[i].id;
                    partsi[i].u = 0;
                    partsi[i].rho = 0;
                    partsi[i].ne = 0;
                    partsi[i].nh = 0;
                    partsi[i].hsml = 0;
                    partsi[i].sfr = 0;
                    partsi[i].age = 0;
                    partsi[i].metz = 0;
                    partsi[i].pot = 0;
                    ++i;
                }
            }

            particles_to_snapshot(partsi, &gal0, type);
        }

        // set the time of the interpolated snapshot.

        gal0.header.time = t;

        // if we are expected to only create images (only_images = true), then
        // we do not need to save this snapshot to disk---only save images.

        if (only_images) // save image to disk
        {
            save_image(&gal0, f + QString::number(interm + 1), config);
        }
        else // save snapshot to disk
        {
            gal0.save_as(f + QString::number(interm + 1));
        }

        clog << "finished with intermediate frame #" << interm + 1 END;
    }

    delete [] parts0;
    delete [] parts1;
    delete [] partsi;
}


void snapshot_to_particles(meta_particle* parts, gadget_io* gal, uint type_id)
{
    uint n_min = 0, c = 0;

    uint n_snap = gal->header.npart[type_id];

    if (type_id > 0)
    {
        for (uint i = 0; i < type_id; ++i)
        {
            n_min += gal->header.npart[i];
        }
    }

    uint max_idx = n_min + n_snap;

    for (uint i = n_min; i < max_idx; ++i)
    {
        // assign positions
        parts[i].r[0] = gal->pos[3 * i];
        parts[i].r[1] = gal->pos[3 * i + 1];
        parts[i].r[2] = gal->pos[3 * i + 2];

        // assign velocities
        parts[i].v[0] = gal->vel[3 * i];
        parts[i].v[1] = gal->vel[3 * i + 1];
        parts[i].v[2] = gal->vel[3 * i + 2];

        // assign particle masses and ID numbers
        parts[i].m = gal->m[i];
        parts[i].id = gal->id[i];

        // assign SPH quantities
        if (type_id == 0)
        {
            parts[i].u = gal->u[i];
            parts[i].rho = gal->rho[i];
            parts[i].ne = gal->ne[i];
            parts[i].nh = gal->nh[i];
            parts[i].hsml = gal->hsml[i];
            parts[i].sfr = gal->sfr[i];
        }
        else
        {
            parts[i].u = 0;
            parts[i].rho = 0;
            parts[i].ne = 0;
            parts[i].nh = 0;
            parts[i].hsml = 0;
            parts[i].sfr = 0;
        }

        // assign ages (actually, these are creation times)
        if (type_id == 4)
        {
            parts[i].age = gal->age[c];
        }
        else
        {
            parts[i].age = 0;
        }

        // assign metallicities
        if (type_id == 0)
        {
            parts[i].metz = gal->metz[i];
        }
        else if (type_id == 4)
        {
            parts[i].metz = gal->metz[gal->header.npart[0] + c];
        }
        else
        {
            parts[i].metz = 0;
        }

        // assign potentials
        parts[i].pot = gal->pot[i];
        ++c;
    }

    meta_particle* begin = parts + n_min;
    meta_particle* end = begin + n_snap;

    sort(begin, end, compare_idx);
}


void particles_to_snapshot(meta_particle* parts, gadget_io* gal, uint type_id)
{
    uint n_min = 0, c = 0;

    // the number of particles of type type_id.
    uint n_snap = gal->header.npart[type_id];

    // determine the index of the first particle of type type_id. This first
    // index is called n_min.
    if (type_id > 0)
    {
        for (uint i = 0; i < type_id; ++i)
        {
            n_min += gal->header.npart[i];
        }
    }

    // determine the upper limit index; the index of the last particle +1 of
    // the current particle type.
    const uint max_idx = n_min + n_snap;

    for (uint i = n_min; i < max_idx; ++i)
    {
        // update positions
        gal->pos[3 * i]   = parts[i].r[0];
        gal->pos[3 * i + 1] = parts[i].r[1];
        gal->pos[3 * i + 2] = parts[i].r[2];

        // update velocities
        gal->vel[3 * i]   = parts[i].v[0];
        gal->vel[3 * i + 1] = parts[i].v[1];
        gal->vel[3 * i + 2] = parts[i].v[2];

        // update masses and ID numbers
        gal->m[i] = parts[i].m;
        gal->id[i] = parts[i].id;

        // update sph quantities
        if (type_id == 0)
        {
            gal->u[i] = parts[i].u;
            gal->rho[i] = parts[i].rho;
            gal->ne[i] = parts[i].ne;
            gal->nh[i] = parts[i].nh;
            gal->hsml[i] = parts[i].hsml;
            gal->sfr[i] = parts[i].sfr;
        }

        // update ages
        if (type_id == 4)
        {
            gal->age[c] = parts[i].age;
        }

        // update metalicities
        if (type_id == 0)
        {
            gal->metz[i] = parts[i].metz;
        }
        else if (type_id == 4)
        {
            gal->metz[gal->header.npart[0] + c] = parts[i].metz;
        }

        // update potentials
        gal->pot[i] = parts[i].pot;
        ++c;
    }
};


bool compare_idx(meta_particle a, meta_particle b) { return (a.id < b.id); }


void save_image(gadget_io* g, QString imagename, image* config)
{
    types my_types;

    if (config->render_gas)
    {
        my_types.gas = 1;
        my_types.bh = 0;
        my_types.bulge = 0;
        my_types.disk = 0;
        my_types.dm = 0;
        my_types.stars = 0;
    }
    else
    {
        my_types = config->my_types;
    }

    float theta = config->theta;
    float phi = config->phi;

    // Now that the types have been specified, create the object

    particle_type_group particles(my_types, g);

    float center_of_mass[7] = {0, 0, 0, 0, 0, 0, 0};

    // We have to enable the automatic updating of the image data using
    // use_rect_mesh() or else the image data will not be updated when the
    // particles are shifted.

    particles.use_rect_mesh(true);

    particles.compute_cm(center_of_mass);

    // shift the origin to center[] of mass with zero net momentum

    center_of_mass[1] = config->center[0];
    center_of_mass[2] = config->center[1];
    center_of_mass[3] = config->center[2];
    particles.shift(center_of_mass);

    if ( (theta != 0) || (phi != 0) ) { particles.rotate(theta, phi); }

    QByteArray str = imagename.toLocal8Bit();

    char* file_name = str.data();

    config->load(&particles);
    config->filename = file_name;
    config->draw();
}
