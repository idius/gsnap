/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
 *  \brief global function and data type / structure definitions.
 */

#include "Core/global.h"

#include <cstdlib>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <ctime>

#ifdef Q_WS_WIN
#include <windows.h>
HANDLE hCon;
#endif


using namespace std;


void gsnap::attention(const char* text, const string& msg)
{
#ifdef Q_WS_WIN
    hCon = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hCon, 4);  // 4 means dark red text.
    cerr << text;
    SetConsoleTextAttribute(hCon, 15); // 15 means white text.
#else
    cerr << "\n\033[1;31m" << text << ": \033[0m";
#endif

    cerr << msg << "\n";
}


void gsnap::print_warning(const string& msg)
{
    gsnap::attention("Warning", msg);
}


void gsnap::print_warning(const char* _msg)
{
    string msg(_msg);
    gsnap::print_warning(msg);
}


void gsnap::print_error(const string& msg, err status)
{
    gsnap::attention("Error", msg);
    exit( static_cast<int>(status) );
}


void gsnap::print_error(const char* _msg, err status)
{
    string msg(_msg);
    gsnap::print_error(msg, status);
}


bool gsnap::is_PNG(const char* file)
{
    ifstream ifs ( file , ifstream::binary );

    // magic number for PNG images
    unsigned char magic[] = {137, 80, 78, 71, 13, 10, 26, 10};

    for (uint i = 0; i < 8; ++i)
    {
        if (magic[i] != ifs.get()) { return false; }
    }

    ifs.close();
    return true;
}


bool gsnap::is_numeric(const char* str)
{
    // This is a somewhat weak test, but probably good enough.
    // A better test would check every char in the array with isdigit().
    return ( ( (str[0] == '-') && isdigit(str[1]) ) || isdigit(str[0]) );
}


float gsnap::randnum()
{
    static bool seeded = false;

    if (!seeded)
    {
        srand(time(NULL));
        seeded = true;
    }

    return static_cast<float>(rand()) / RAND_MAX;
}
