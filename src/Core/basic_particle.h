/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
    \brief Contains the basic_particle class for storing any type of particle.
*/

#ifndef BASIC_PARTICLE_H
#define BASIC_PARTICLE_H

#include "Core/global.h"


struct massive_particle;
struct stellar_particle;
struct sph_particle;


/*! \brief Stores all data for any individual particle: stars, dark matter, gas,
           and black holes.

    This structure stores all of the data for an individual particle. Storing
    the data in this way allows us to easily sort particles while automatically
    keeping track of their individual properties rather than storing each type
    of quantity in a separate array. */
struct meta_particle
{
    pcat   species;
    kpc    r[3];
    float  v[3];
    float  a[3];
    float  m;
    int    id;
    float  u;
    float  rho;
    float  ne;
    float  nh;
    float  hsml;
    float  eps;
    float  sfr;
    float  age;
    float  metz;
    float  pot;
    meta_particle();
    meta_particle(const meta_particle& s);
    meta_particle(const massive_particle& s) { from(s); }
    meta_particle(const stellar_particle& s) { from(s); }
    meta_particle(const sph_particle& s) { from(s); }
    void from(const massive_particle& s);
    void from(const stellar_particle& s);
    void from(const sph_particle& s);
    void to(massive_particle& s);
    void to(stellar_particle& s);
    void to(sph_particle& s);
};


struct massive_particle
{
    pcat   species;
    kpc    r[3];
    float  v[3];
    float  a[3];
    float  m;
    int    id;
    float  pot;
    massive_particle();
    massive_particle(const massive_particle& s);
};


struct stellar_particle
{
    kpc    r[3];
    float  v[3];
    float  a[3];
    float  m;
    int    id;
    float  age;
    float  metz;
    float  pot;
    stellar_particle();
    stellar_particle(const stellar_particle& s);
    stellar_particle(const massive_particle& s);
    stellar_particle(const sph_particle& s);
};


struct sph_particle
{
    kpc    r[3];
    float  v[3];
    float  a[3];
    float  m;
    int    id;
    float  u;
    float  rho;
    float  ne;
    float  nh;
    float  hsml;
    float  sfr;
    float  metz;
    float  pot;
};


/*! \brief Stores a subset of the SPH data for an individual particle.

    This is useful for manipulating and analyzing SPH particles because it
    requires much less memory per particle than using the more general
    meta_particle and sph_particle classes. When high performance is required,
    use this. */
struct compact_sph
{
    kpc   r[3];
    float m;
    float met;
    float u;
    compact_sph();
    compact_sph(const compact_sph& s);
    compact_sph operator=(const compact_sph& s);
};


/*! \brief Stores a subset of the data for an individual stellar particle.

    This is useful for visualizing the stellar particles in a snapshot because
    it requires less memory than the more general structures, which allows us to
    push more particles through the processor per unit time. Furthermore, it
    contains entries for the red and blue fluxes. */
struct color_particle
{
    kpc   r[3];
    float blue;
    float red;
    color_particle();
    color_particle(const color_particle& s);
    color_particle operator=(const color_particle& s);
};


/*! \brief A bit-field for specifying which particle types are included in the
    particle_type_group object. */
struct types
{
    unsigned gas:   1;   // gas/SPH particles
    unsigned dm:    1;   // dark matter halo particles
    unsigned disk:  1;   // disk star particles
    unsigned bulge: 1;   // bulge star particles
    unsigned stars: 1;   // newly-created star particles
    unsigned bh:    1;   // black hole particles
};

#endif
