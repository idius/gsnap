/* the scripting language is ECMAScript with the addition of custom
functions * which expose part of GSnap's internal machinery. The
ECMAScript language * is documented here:
*
          http://www.ecma-international.org/ecma-262/5.1/  */

//********************** GSnap-specific Commands ***********************

// specify a snapshot filename

var snapshot_filename = "../snapshots/snapshot_300";

// Before loading a snapshot, specify which particle types to load.
// There are two options:

particle_types("101110"); // using a string specifier, similar to command line interface

particle_types(Gas | DiskStar | BulgeStar | NewStar) // using explicit type names in an OR chain
                                                     // (i.e., separated with |)

// load the snapshot

load_snapshot(snapshot_filename)

// store the snapshot time in the variable, "time"

var time = snapshot_time();

print( "time =", time, "\n" )

// set slit dimensions and orientation

var width = 2;
var length = 20;
var alpha = 0;

set_slit(width, length, alpha); // or simply set_slit(2, 20, 0);

// center the system at x = 39, y = 36, z = 0

center_on(39, 36, 0)

// rotate the system to theta = 0.18, phi = 0.4

rotate_to(0.18, 0.4)

// center the system at x = 0.1, y = 0.2, z = 0.3

center_on(0.1, 0.2, 0.3)

// specify the age range of the stellar particles that you want to measure

//set_age_bin(0, 1)

// compute velocity dispersion

var sig = sigma();

print("velocity dispersion =", sig, "km/s.\n");


// ******************** ECMAScript Basics By Eample ********************


// declaring variables. The var keyword limits the variable's scope

var local_variable1 = 1; // declare and define;

var local_variable2;

print(local_variable1)

localvariable2 = 1;

global_variable = 2; // without var, the variable is visible outside
                     // of the code block in which it was declared.


// some basic math examples:

print("sin(1) =", Math.sin(1), "\n")

print("atan2(-1, -2) =", Math.atan2(-1,-2), "\n")


// a function definition:

function magnitude(x, y, z)
{
    return Math.sqrt(x*x + y*y + z*z);
}

// using the function:

var mag111 = magnitude(1, 1, 1);

print("the magnitude of [1, 1, 1] is", mag111);

// alternative forms of the previous statement:

print("the magnitude of [1, 1, 1] is " + mag111.toString() );

print("the magnitude of [1, 1, 1] is " + mag111.toString().slice(0,5) );

print("the magnitude of [1, 1, 1] is "
      + mag111.toString().substring(0,5) );

print("the magnitude of [1, 1, 1] is ".toUpperCase()
      + mag111.toString().slice(0,5) );

// convert string to number after making manipulations

var my_number_string = mag111.toString()

my_number_string = "1" + my_number_string;

var my_number = Number(my_number_string);

print("my_number is" , my_number);


// Arrays

var mass = new Array()

mass[0] = 5.5;
mass[1] = 2.52;
mass[2] = 9.3;

// one way print elements of the mass array (note the for-in syntax)

print("\nmass :\n")

for ( m in mass )
{
    print(mass[m])
}

// another way to print the contents of the mass array

print ("\nanother way to print mass:\n")

print(mass)

// arrays can contain mixed data types. They also support concatenation,
// sorting, splicing, slicing, and they have pop() and push() methods.


// another form of the for loop

for (var i = 0; i < 20; ++i)
{
    // conditional branch demonstration
    if (i == 0) { print("\nanother form of the for loop:\n") }

    mass[i] = Math.pow(i, 0.6);

    print(mass[i]);
}


// Custom Objects are defined using their constructor. Here's a prototype
// for a simple Particle object:

function Particle(x, y, z, vx, vy, vz, m, id)
{
    this.x = x;
    this.y = y;
    this.z = z;
    this.vx = vx;
    this.vy = vy;
    this.vz = vz;
    this.m = m;
    this.id = id;
}

// add a method to Particle for computing momentum:

Particle.prototype.momentum = function()
{
    var px = this.m * this.vx;
    var py = this.m * this.vy;
    var pz = this.m * this.vz;
    return new Array(px, py, pz);
};

// create a Particle object

var p = new Particle(0.14, 1.2, 1.1, 4.6, 2.1, 5.5, 0.4, 10);

print("\np.m =", p.m);

print( "\nthe momentum of p is [", p.momentum(),"]\n" )

// Exercise: Create an array of particle objects with randomly-selected
// masses, velocities, and positions, but sequential particle IDs.

// With these ingredients, it is technically possible to write an N-body
// code for evolving a system forward in time! Performance would be horrible,
// but it is POSSIBLE!
