/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
  \brief Implements a basic scripting interface using Qt's scripting tools.

 ******************************************************************************/

#ifndef SCRIPT_READER_H
#define SCRIPT_READER_H

#include "Core/global.h"
#include "Core/particles.h"
#include "Viz/image.h"
#include "CLI/cli_actions.h"

#include <QtScript>

class QString;



/*! \brief Reads a script file and performs the requested actions. */
class script_reader
{
private:

    typedef QScriptValue QSValue;

    typedef QScriptEngine QSEngine;

    typedef QScriptContext QSContext;

    QString script_filename;

    QSEngine engine;

    static types my_types;

    static gadget_io* gal;

    static particle_type_group* particles;

public:

    script_reader(types _my_types,
                  const char* _script_file);

    ~script_reader();

private:

    void parse_script();

    void setup_engine();

    void particles_error();

    static void check_numerical_arguments(QSContext* _context, uint nargs);

    static QSValue load_snapshot(QSContext* _context, QSEngine* _engine);

    static QSValue particle_types(QSContext* _context, QSEngine* _engine);

    static QSValue gsnap_version(QSContext* _context, QSEngine* _engine);

    static QSValue snapshot_time(QSContext* _context, QSEngine* _engine);

    static QSValue set_slit(QSContext* _context, QSEngine* _engine);

    static QSValue sigma(QSContext* _context, QSEngine* _engine);

    static QSValue rotate_to(QSContext* _context, QSEngine* _engine);

    static QSValue center_on(QSContext* _context, QSEngine* _engine);

    static QSValue set_age_bin(QSContext* _context, QSEngine* _engine);

    static QSValue close_snapshot(QSContext* _context, QSEngine* _engine);

    // not yet implemented:

    static QSValue center_of_mass(QSContext* _context, QSEngine* _engine);

    static QSValue sigma_stats(QSContext* _context, QSEngine* _engine);

    static QSValue render_gas(QSContext* _context, QSEngine* _engine);

    static QSValue render_flux(QSContext* _context, QSEngine* _engine);

    static QSValue render_particles(QSContext* _context, QSEngine* _engine);

    static QSValue get_bh_separation(QSContext* _context, QSEngine* _engine);

    static QSValue parameters(QSContext* _context, QSEngine* _engine);

    static QSValue load_parameters(QSContext* _context, QSEngine* _engine);

    static QSValue interpolate(QSContext* _context, QSEngine* _engine);

    static QSValue get_snapshot_info(QSContext* _context, QSEngine* _engine);
};

#endif // SCRIPT_READER_H
