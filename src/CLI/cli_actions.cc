/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
 *  \brief Contains cli_actions member function definitions.
 *  \todo Eventually, all of the larger tasks should be extracted and placed
 *        into their own files. For instance, view() and parse_view() should be
 *        in their own files, as should all of the computation tasks,
 *        interpolation, and the modification utilities.
 */

#include "GUI/gui_preview.h"
#include "CLI/cli_actions.h"
#include "CLI/script_reader.h"
#include "CLI/param_reader.h"
#include "Core/particles.h"
#include "Core/interpolate.h"
#include "IO/io.h"

#include <QString>
#include <QStringList>

#include <cmath>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <sstream>
#include <cctype>

#pragma GCC optimize ("Os")

using namespace std;

cli_actions::cli_actions(int _argc, char** _argv)
{
    argc = _argc;
    argv = _argv;

    // Associate strings with enumeration entries.

    make_flag_map();

    // read the parameter file:

    parameter_file = getenv ("GSNAP_PARS");

    if (parameter_file != NULL)
    {
        param_reader parameters(this);
        parameters.parse_file(parameter_file);
    }
}


void cli_actions::parse_cl()
{
    uint arg_number = 1;

    while (arg_number < uint(argc) )
    {
        if ( is_char_flag(argv[arg_number]) )
        {
            arg_number += interpret_char_flags(arg_number);
        }
        else if ( is_long_flag(argv[arg_number]) )
        {
            arg_number += interpret_long_flags(arg_number);
        }
        else if (argc == 2) // No flags were found; assume argv[1] is file_name
        {
            input_file0 = argv[1];
            my_task = computation;
            ++arg_number;
        }
        else if ( arg_number == (uint(argc) - 1) )
        {
            // assume the final argument is the file name, input_file0.

            char* arg = argv[arg_number];
            arg_error(is_char_flag(arg), arg);

            input_file0 = arg;
            ++arg_number;
        }
        else
        {
            string msg = "There is probably a syntax error. I don't understand"
                         " what you mean by \"" + string(argv[arg_number])
                         + "\". I'll try to ignore it, but I thought you should"
                         " know about this potential problem.\n";

            gsnap::print_warning(msg);
            ++arg_number;
        }
    }

    run();
}


void cli_actions::run()
{
    switch (my_task)
    {
    case interpolation: interp();          break;
    case visualization: view();            break;
    case computation:   proj_stats();      break;
    case information:   info();            break;
    case modification:  modify_snapshot(); break;
    }

    exit(0);
}


void cli_actions::set_center(const uint c)
{
    shift[0] = (float)atof(argv[c]);
    shift[1] = (float)atof(argv[c + 1]);
    shift[2] = (float)atof(argv[c + 2]);
    auto_center = false;

    img.center[0] = shift[0];
    img.center[1] = shift[1];
    img.center[2] = shift[2];
}

void cli_actions::make_flag_map()
{
    flag_map["--phi"     ] = flag::phi;
    flag_map["--theta"   ] = flag::theta;
    flag_map["--view"    ] = flag::view;
    flag_map["--frames"  ] = flag::frames;
    flag_map["--beg"     ] = flag::beg;
    flag_map["--end"     ] = flag::end;
    flag_map["--out"     ] = flag::out;
    flag_map["--version" ] = flag::version;
    flag_map["--parfile" ] = flag::parfile;
    flag_map["--param"   ] = flag::param;
    flag_map["--prob"    ] = flag::prob;
    flag_map["--dof"     ] = flag::dof;
    flag_map["--which"   ] = flag::which;
    flag_map["--sigma"   ] = flag::sigma;
    flag_map["--types"   ] = flag::types;
    flag_map["--center"  ] = flag::center;
    flag_map["--info"    ] = flag::info;
    flag_map["--script"  ] = flag::script;
    flag_map["--speed"   ] = flag::speed;
    flag_map["--age_bin" ] = flag::age_bin;
    flag_map["--modify"  ] = flag::modify;
}


uint cli_actions::interpret_long_flags(const uint arg_n)
{
    param_reader parameters(this);

    string opt_string(argv[arg_n]);

    char* arg;

    string msg;

    switch ( flag_map[opt_string] )
    {
    case flag::error:

        msg += "I don't recognize the flag \"" + opt_string + "\"\n";
        gsnap::print_error(msg, err::syntax);

    case flag::phi:

        arg = check_bounds(arg_n + 1);
        arg_error(!gsnap::is_numeric(arg), arg);

        phi = (float)atof(argv[arg_n + 1]);
        return 2;
        break;

    case flag::theta:

        arg = check_bounds(arg_n + 1);
        arg_error(!gsnap::is_numeric(arg), arg);

        theta = (float)atof(argv[arg_n + 1]);
        return 2;
        break;

    case flag::view:

        arg = check_bounds(arg_n + 1);
        parse_view(arg);
        return 2;
        break;

    case flag::frames:

        arg = check_bounds(arg_n + 1);
        arg_error(!gsnap::is_numeric(arg), arg);

        N_inter = (uint)atoi(argv[arg_n + 1]);
        return 2;
        break;

    case flag::beg:

        arg = check_bounds(arg_n + 1);
        arg_error(is_char_flag(arg), arg);

        input_file0 = arg;
        return 2;
        break;

    case flag::end:

        arg = check_bounds(arg_n + 1);
        arg_error(is_char_flag(arg), arg);

        input_file1 = arg;
        return 2;
        break;

    case flag::out:

        arg = check_bounds(arg_n + 1);
        arg_error(is_char_flag(arg), arg);

        output_file = arg;
        return 2;
        break;

    case flag::version:

        cout << "\nThis is GSnap version " << VERSION << "\n\n";
        cout << "Copyright (C) 2013 Nathaniel R. Stickley (idius@idius.net)\n"
                "GSnap is free software; see the source (http://gsnap.org)\n"
                "for copying conditions. There is NO warranty; not even \n"
                "for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n\n";
        exit(0);
        break;

    case flag::parfile:

        arg = check_bounds(arg_n + 1);
        arg_error(is_char_flag(arg), arg);

        parameter_file = argv[arg_n + 1];
        parameters.parse_file(parameter_file);
        return 2;
        break;

    case flag::param:

        arg = check_bounds(arg_n + 1);
        arg_error(is_char_flag(arg), arg);

        parameters.parse_line(arg);
        return 2;
        break;

    case flag::prob:

        my_task = computation;
        prob_histogram = true;
        sigma_stats = false;
        return 1;
        break;

    case flag::dof:

        arg = check_bounds(arg_n + 1);
        arg_error(!gsnap::is_numeric(arg), arg);

        img.DepthOfField = (float)atof(argv[arg_n + 1]);

        if ( img.DepthOfField > 1 || img.DepthOfField < 0)
        {
            gsnap::print_error("The depth of field must be in the range [0,1]."
                               "\n", err::syntax);
        }

        return 2;
        break;

    case flag::which:

        arg = check_bounds(arg_n + 1);
        arg_error(!gsnap::is_numeric(arg), arg);

        which_bh = (uint)atoi(argv[arg_n + 1]);
        nucleus_centered = true;

        return 2;
        break;

    case flag::sigma:

        my_task = computation;
        single_sigma = true;
        sigma_stats = false;
        return 1;
        break;

    case flag::types:

        // This flag requires an argument of numeric type.
        arg = check_bounds(arg_n + 1);
        arg_error(!gsnap::is_numeric(arg), arg);

        set_types(arg_n + 1);
        return 2;
        break;

    case flag::center:

        // this flag requires three arguments of numeric type
        arg = check_bounds(arg_n + 1);
        arg_error(!gsnap::is_numeric(arg), arg);
        arg = check_bounds(arg_n + 2);
        arg_error(!gsnap::is_numeric(arg), arg);
        arg = check_bounds(arg_n + 3);
        arg_error(!gsnap::is_numeric(arg), arg);

        set_center(arg_n + 1);
        return 4;
        break;

    case flag::info:

        check_bounds(arg_n + 1);
        my_task = information;
        return 1;
        break;

    case flag::script:

        arg = check_bounds(arg_n + 1);

        if (arg)
        {
            execute_script(my_types, arg);
            exit(0);
        }

        return 2;
        break;

    case flag::speed:

        my_task = computation;
        sigma_stats = false;
        speed = true;
        return 1;
        break;

    case flag::age_bin:

        // this flag requires two arguments of numeric type
        arg = check_bounds(arg_n + 1);
        arg_error(!gsnap::is_numeric(arg), arg);
        arg = check_bounds(arg_n + 2);
        arg_error(!gsnap::is_numeric(arg), arg);

        my_task = computation;
        sigma_stats = true;
        age_bin[0] = float( atof( check_bounds(arg_n + 1) ) );
        age_bin[1] = float( atof( check_bounds(arg_n + 2) ) );

        if ( (age_bin[1] - age_bin[0]) <= 0)
        {
            gsnap::print_error("You entered an invalid age bin. The bin must "
                               "be given in the form low_end, high_end, where "
                               "(high_end - low_end) > 0.", err::syntax);
        }

        return 3;
        break;

    case flag::modify:

        arg = check_bounds(arg_n + 1);
        parse_modify(arg);
        return 2;
        break;

    default:

        return 1;
    }
}


uint cli_actions::interpret_char_flags(const uint arg_number)
{
    uint string_idx = 1;
    char opt, *arg;
    string msg;

    do
    {
        opt = argv[arg_number][string_idx];

        switch (opt)
        {
        case 'g':  // render gas

            gas = true;
            my_task = visualization;
            ++string_idx;
            break;

        case 'i': // interpolated snapshots

            only_images = false;
            my_task = interpolation;
            ++string_idx;
            break;

        case 'I': // interpolated images

            only_images = true;
            my_task = interpolation;
            ++string_idx;
            break;

        case 'w': // slit width

            // This flag requires one argument. Does the argument exist?
            arg = check_bounds(arg_number + 1);

            // The argument must be of numeric type. Is it?;
            arg_error(!gsnap::is_numeric(arg), arg);

            slit_width = (float)atof(argv[arg_number + 1]);
            return 2;
            break;

        case 'l': // slit length

            arg = check_bounds(arg_number + 1);
            arg_error(!gsnap::is_numeric(arg), arg);

            slit_length = (float)atof(argv[arg_number + 1]);
            return 2;
            break;

        case 't': // type selection

            arg = check_bounds(arg_number + 1);
            arg_error(!gsnap::is_numeric(arg), arg);

            set_types(arg_number + 1);
            return 2;
            break;

        case 'a': // age cutoff

            arg = check_bounds(arg_number + 1);
            arg_error(!gsnap::is_numeric(arg), arg);

            age_cut = (float)atof(argv[arg_number + 1]);
            return 2;
            break;

        case 'n': // nucleus-centered

            nucleus_centered = true;
            my_task = computation;
            ++string_idx;
            break;

        case 'c': // center on position

            arg = check_bounds(arg_number + 1);
            arg_error(!gsnap::is_numeric(arg), arg);
            arg = check_bounds(arg_number + 2);
            arg_error(!gsnap::is_numeric(arg), arg);
            arg = check_bounds(arg_number + 3);
            arg_error(!gsnap::is_numeric(arg), arg);

            set_center(arg_number + 1);
            return 4;
            break;

        case 's': // compute velocity dispersion along theta, phi direction

            my_task = computation;
            sigma_stats = false;
            ++string_idx;
            break;

        default:

            if (opt == '\0')
            {
                return 1;
            }
            else
            {
                msg += "I don't recognize the flag \"" + string(&opt) + "\"\n";
                gsnap::print_error(msg, err::syntax);
            }
        }
    }
    while (opt != '\0');

    return 1;
}


void cli_actions::interpret_types()
{
    my_types.gas   = my_types.dm   = my_types.bh    = 0;
    my_types.bulge = my_types.disk = my_types.stars = 0;

    uint type_spec = 0, i = 0;

    while (*types_string != '\0' &&  type_spec < 6)
    {
        switch (type_spec)
        {

        case 0: if (types_string[i] == '1') { my_types.gas = 1; }   break;

        case 1: if (types_string[i] == '1') { my_types.dm = 1; }    break;

        case 2: if (types_string[i] == '1') { my_types.disk = 1; }  break;

        case 3: if (types_string[i] == '1') { my_types.bulge = 1; } break;

        case 4: if (types_string[i] == '1') { my_types.stars = 1; } break;

        case 5: if (types_string[i] == '1') { my_types.bh = 1; }    break;
        }

        ++i;
        ++type_spec;
    }
}


void cli_actions::shift_snapshot_center(particle_type_group& _particles)
{
    float center_of_mass[7] = {0, 0, 0, 0, 0, 0, 0};

    // We have to enable the automatic updating of the image data using
    // use_rect_mesh() or else the image data will not be updated when the
    // particles are shifted.

    _particles.use_rect_mesh(true);

    _particles.compute_cm(center_of_mass);

    if (auto_center)
    {
        _particles.shift(center_of_mass);
    }
    else
    {
        center_of_mass[1] = shift[0];
        center_of_mass[2] = shift[1];
        center_of_mass[3] = shift[2];
        _particles.shift(center_of_mass);
    }
}


bool cli_actions::is_char_flag(const char* str) const
{
    return ( (str[0] == '-') && isalpha(str[1]) );
}


bool cli_actions::is_long_flag(const char* str) const
{
    return ( (str[0] == '-') && (str[1] == '-') && isalpha(str[2]) );
}


void cli_actions::arg_error(const bool b, const char* str) const
{
    if (b)
    {
        string msg = "\"" + string(str) + "\" is an invalid argument.\n";
        gsnap::print_error(msg, err::syntax);
    }
}


char* cli_actions::check_bounds(const uint n) const
{
    if (n < (uint)argc)
    {
        return argv[n];
    }
    else
    {
        gsnap::print_error("There appears to be a missing argument.",
                           err::syntax);
    }
}


void cli_actions::info() const
{
    gadget_io gal(input_file0);

    cout << "time: "          << gal.header.time END;
    cout << "N_total: "       << gal.Ntot END;
    cout << "Gas: "           << gal.Ngas END;
    cout << "Dark Matter: "   << gal.Ndm END;
    cout << "Disk Stars: "    << gal.header.npart[2] END;
    cout << "Bulge Stars: " << gal.header.npart[3] END;
    cout << "New Stars: "   << gal.header.npart[4] END;
    cout << "Black Holes: " << gal.Nbh END;
    cout << "Total Mass: "    << gal.Mtot END;
}


void cli_actions::proj_stats()
{
    // open the file (store contents in the snapshot object, gal)

    gadget_io gal(input_file0);

    // load the appropriate particles into a particle_type_group object.

    interpret_types();

    particle_type_group particles(my_types, &gal);

    // shift the origin of the system to the desired position and
    // velocity. The final system has zero net momentum.

    float new_origin[7] = {0, 0, 0, 0, 0, 0, 0};

    if (!(nucleus_centered && particles.find_bh(which_bh, new_origin)))
    {
        particles.compute_cm(new_origin);
    }

    particles.shift(new_origin);

    // Filter particles based on their ages

    if (age_cut)
    {
        if (abs(age_cut) > gal.header.time)
        {
            string msg = "You entered an invalid age ";
            msg += "(" + to_string(age_cut) + ")\n";
            msg += "The time of the current snapshot is ";
            msg += to_string(gal.header.time);
            gsnap::print_error(msg, err::syntax);
        }

        if (age_cut > 0)
        {
            age_cut = gal.header.time - age_cut;
        }
        else
        {
            if (age_cut < 0)
            {
                age_cut = -1.0 * (gal.header.time + age_cut);
            }
        }
    }

    particles.set_age_cutoff(age_cut);

    particles.set_age_bin(age_bin);

    // Perform the specified computations

    if (sigma_stats)
    {
        particles.set_N_projections(N_projections);

        particles.projection_statistics(slit_width, slit_length);
    }
    else if (prob_histogram)
    {
        particles.set_N_projections(N_projections);

        particles.set_slit(slit_width, slit_length);

        particles.sigma_probability_histogram();
    }
    else if (speed)
    {
        // a temporary piece of code for the paper (Stickley_Canalizo 2)
        particles.collide_on_z();

        //particles.find_bh_rel_speed();
    }
    else if (single_sigma)
    {
        shift_snapshot_center(particles);

        particles.set_slit(slit_width, slit_length);

        particles.rotate(theta, phi);

        cout.precision(8);
        cout.setf(ios::fixed, ios::floatfield);
        cout << particles.sigma() END;
    }
}


void cli_actions::view()
{
    const char gas_type[] = "100000";

    // open the file

    gadget_io gal(input_file0);

    // load the appropriate particles into a particle_type_group object.

    if (gas)
    {
        for (uint i = 0; i < 6; ++i) { types_string[i] = gas_type[i]; }
    }

    interpret_types();

    particle_type_group particles(my_types, &gal);

    particles.set_age_bin(age_bin);

    // center the particle group:

    shift_snapshot_center(particles);

    // In the --view particles option, rotation is performed at the time
    // of projection, so we only have to set the direction. In the other cases,
    // the rotation has to be performed here.

    if ( (theta != 0) || (phi != 0) )
    {
        if (gas || interactive || light || stars)
        {
            particles.rotate(theta, phi);
        }
        else
        {
            particles.set_direction(theta, phi);
        }
    }

    if (!interactive)
    {
        img.load(&particles);
        img.theta = theta;
        img.phi = phi;
        img.render_gas = gas;
        img.render_light = light;
        img.render_stars = stars;
        img.filename = input_file0;
        img.draw();
    }
    else
    {
        int i = 4;
        QApplication app(i, argv);// preview window does not work without this

        gui_preview gui(&particles, 0);

        gui.show();

        gui.exec();
    }
}


void cli_actions::interp()
{
    const bool ready = (input_file0 && input_file1 && output_file);

    if (!ready)
    {
        gsnap::print_error("You must specify two input files and an output "
                           "file name using --beg, --end, and --out.\n",
                           err::syntax);
    }

    img.theta = theta;
    img.phi = phi;
    img.center[0] = shift[0];
    img.center[1] = shift[1];
    img.center[2] = shift[2];
    img.render_gas = gas;
    img.render_light = light;

    interpret_types();

    img.my_types = my_types;

    interpolate(input_file0,
                input_file1,
                output_file,
                N_inter,
                &img,
                only_images);
}


void cli_actions::modify_snapshot()
{
    gadget_io gal(input_file0);

    my_types.bh = my_types.gas = my_types.dm = 1;
    my_types.disk = my_types.bulge = my_types.stars = 1;

    particle_type_group particles(my_types, &gal);

    // perform modifications here. It is preferable to make the modifications
    // to the particle_type_group object, particles, then save the modifications
    // to the snapshot using particles.renew_snapshot()

    cerr << "perform modifications.\n";

    const meta_particle* disk = particles.get_snap_pointer(pcat::DiskStar);
    const uint ndisk = particles.get_n_type(pcat::DiskStar);

    // convert all disk particles to new star particles
    for (uint i = 0; i < ndisk; ++i)
    {
        meta_particle dp = disk[i];
        dp.metz = 0.025;
        dp.age = -9.0;
        particles.push_back(dp, pcat::NewStar);
    }

    // remove the disk star particles
    for (uint i = 0; i < ndisk; ++i)
    {
        particles.pop_back(pcat::DiskStar);
    }

    const meta_particle* bulge = particles.get_snap_pointer(pcat::BulgeStar);

    const uint nbulge = particles.get_n_type(pcat::BulgeStar);

    // convert all bulge particles to new star particles
    for (uint i = 0; i < nbulge; ++i)
    {
        meta_particle bp = bulge[i];
        bp.metz = 0.02;
        bp.age = -9.0;
        particles.push_back(bp, pcat::NewStar);
    }

    // remove the bulge star particles
    for (uint i = 0; i < nbulge; ++i)
    {
        particles.pop_back(pcat::BulgeStar);
    }

    const meta_particle* newstar = particles.get_snap_pointer(pcat::NewStar);

    const uint nnewstar = particles.get_n_type(pcat::NewStar);

    for (uint i = 0; i < nnewstar; ++i)
    {
        const float z = newstar[i].metz;

        if (z > 0.04)
        {
            particles.set_particle_metalicity(pcat::NewStar, i, 0.4 * z);
        }
    }

    particles.renew_snapshot();

    gal.set_file_format(snap_format::Gadget2);

    if (output_file)
    {
        gal.save_as(output_file);
    }
    else
    {
        gsnap::print_error("No output file name was specified! Use --out to "
                           "specify a file name.", err::syntax);
    }
}


void cli_actions::parse_view(const char* view_method)
{
    QString method(view_method);

    if ( method == "flux" || method == "f" )
    {
        interactive = false;
        my_task = visualization;
        light = true;
    }
    else if ( method == "gas" || method == "g" )
    {
        interactive = false;
        my_task = visualization;
        gas = true;
    }
    else if ( method == "particles" || method == "p" )
    {
        interactive = false;
        my_task = visualization;
    }
    else if ( method == "interactive" || method == "i" )
    {
        interactive = true;
        my_task = visualization;
    }
    else if (method == "stars" || method == "s" )
    {
        interactive = false;
        my_task = visualization;
        stars = true;
    }
    else
    {
        gsnap::print_error("I do not recognize the viewing method that you "
                           "specified: " + string(view_method) + "\n",
                           err::syntax);
    }
}


void cli_actions::parse_modify(const char* mod)
{
    my_task = modification;

    QString mod_type(mod);

    if ( mod_type == "remove" || mod_type == "rm" )
    {
        modify_task = modify::remove;
    }
    else if ( mod_type == "remove_type" || mod_type == "rm_type")
    {
        modify_task = modify::remove_type;
    }
    else if ( mod_type == "convert" )
    {
        // note that this requires us to specify
        // two types (a FROM type and a TO type).
        modify_task = modify::convert;
    }
    else if ( mod_type == "time" )
    {
        modify_task = modify::time;
    }
    else
    {
        modify_task = modify::none;
    }
}


void cli_actions::execute_script(types _types, char* _filename)
{
    QCoreApplication app(argc, argv); // required for the script reader to work.
    script_reader script(_types, _filename);
    exit(0);
    app.exec();
}
