/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
  \brief Contains the cli_actions class, which implements the command line
  interface.

  \todo Re-implement the age_cut using the age_bin method.

  \todo implement a --combine flag for combining snapshots.

  \todo generalize the long flag input to make it even easier to add a flag.

  \todo Write documentation / instructions for adding a new command line option.

 ******************************************************************************/

#ifndef CLI_ACTIONS_H
#define CLI_ACTIONS_H

#include "Core/global.h"
#include "Core/particles.h"
#include "CLI/param_reader.h"
#include "Viz/image.h"

#include <cstdlib>
#include <iostream>
#include <map>
#include <string>


/*! \brief Reads the command line flags and performed the corresponding actions.

    This class contains the command line interface. An object of this class can
    parse the command line flags and perform actions based on the flags and  */
class cli_actions
{
public:

    image img; /*!< Stores all image creation settings. */

    kpc slit_width {SLIT_WIDTH}; /*!< The diffraction slit mask width. */

    kpc slit_length {SLIT_HEIGHT}; /*!< The diffraction slit mask length. */

    /*! The number of projections used when computing statistics.*/
    uint  N_projections {NP};

protected:

    int argc;
    char** argv;

    // file names
    char* input_file0 {nullptr};
    char* input_file1 {nullptr};
    char* output_file {nullptr};
    char* parameter_file {nullptr};

    /// the viewing direction, measured in radians
    rad theta {0};

    /// the viewing direction, measured in radians
    rad phi {0};

    /// x,y,z coordinates of the center of the viewing box.
    float shift[3] {0, 0, 0};

    /*! Determines which stars will be included in the velocity dispersion
        measurements. If age_cut > 0, particles younger than age_cut will be
        examined. If age_cut < 0, particles older than age_cut are examined. If
        age_cut = 0, particles of all ages are included.*/
    float age_cut {0};

    /*! \brief Sets the {minimum, maximum} ages of stars to analyze.

        Determines which stars will be included in the velocity dispersion
        measurements. Only stars with ages between age_bin[0] and age_bin[1] are
        included.*/
    float age_bin[2] {0, 0};

    ///  The number of intermediate images or snapshots that will be produced by
    ///  the interpolation scheme.
    uint N_inter {1};

    ///  specifies which black hole particle the velocity dispersion measurements
    ///  will center upon if `nucleus_centered == true`.
    uint which_bh {0};

    /// indicates which particle types are included in the task performed.
    char types_string[6] {'0', '0', '1', '1', '1', '0'};

    /*! specifies which particle types will be included in the task performed.*/
    types my_types;

    ///  if true, automatically center the snapshot when viewing.
    ///  If false, the center must be specified with the `shift[]` member.
    bool auto_center {true};

    /// if true, only output interpolated images when performing interpolation.
    /// if false, output interpolated snapshot files.
    bool only_images {false};

    ///  if false, center velocity dispersion measurements on the center of mass.
    ///  if true, center velocity dispersion measurements on a black hole.
    bool nucleus_centered {false};

    ///  if true, the view() method performs volume rendering on the gas.
    ///  if false, view() displays the particle types specified in my_types.
    bool gas {false};


    bool stars {false};

    /// if true, the view() method performs volume rendering using starlight
    /// as the pixel intensity. Gas in the snapshot attenuates starlight.
    bool light {false};

    ///  if true, view() launches the interactive preview window.
    ///  if false, view() saves an image file.
    bool interactive {false};

    ///  if true, compute sigma statistics.
    bool sigma_stats {true};

    /// if true, compute velocity dispersion along the theta, phi direction.
    bool single_sigma {false};

    /// if true, compute a histogram of the velocity dispersion distribution.
    bool prob_histogram {false};

    /// if true, compute the relative speed of the first two black holes in the
    /// snapshot.
    bool speed {false};

    ///  specifies the general types of tasks performed by GSnap.
    enum tasks
    {
        interpolation,
        visualization,
        computation,
        information,
        modification
    } my_task {computation};

    /*!  This enumeration, along with a std::map<string, string_flags>, object
        allows us to use strings as values in a switch statement. Refer to
        interpret_long_flags() to see exactly how this is done. */
    enum class flag
    {
        error,
        phi,
        theta,
        view,
        frames,
        beg,
        end,
        out,
        version,
        parfile,
        param,
        prob,
        dof,
        which,
        sigma,
        types,
        center,
        info,
        script,
        speed,
        age_bin,
        modify
    };

    ///   An associative array for connecting string flags with elements of the
    ///   long_flag enumeration.
    std::map< std::string, flag > flag_map;

    /// Enumeration or specifying which sort of modification will be performed
    /// on the snapshot.
    enum class modify
    {
        none,        /*!< Perform no changes. */
        remove,      /*!< Remove individual particles from the snapshot. */
        remove_type, /*!< Remove an entire type of particles from a snapshot. */
        convert,     /*!< convert particles of one type into another type. */
        time         /*!< Change the time of the snapshot. */
    } modify_task;

public:

    /*! \brief Reads the command line arguments.

    \param _argc the number of command line arguments.
    \param _argv array of strings containing the command line arguments.*/
    cli_actions(int _argc, char** _argv);

    /*! \brief Parses the command line arguments.


    Reads the arguments in argv. Interprets any argument beginning with '`-`'
     or '`--`' as a special keyword.

     In general, there are two types of keyword functions. Some specify an
     input value and others specify an action.  For instance, in the
     current implementation, the `-g`, `-n`, `--view`, `-i`, `--iview` flags
     only indicate that an action should be performed, while the `-t`, `-a`,
     `--beg`, `--end`, `--out`, `--theta`, `--phi`, `-c`, and `--frames` flags
     specify that the next argument on the command line is to be interpreted as
     an input value of some kind.

     Note that we strictly follow a naming convention whereby flags beginning
     with '`-`' are single character, "short" flags, while flags beginning with
     '`--`' are "long" flags, consisting of a string.

     Short flags can be combined. For instance, rather than using

     `./gsnap -n -t 001110 filename`

     it is sufficient to use

     `./gsnap -nt 001110 filename.`

     However, input flags cannot be followed by another flag. For instance,

     `./gsnap -tn 001110 filename.`

     Will not yield the expected result because the `-n` flag will be ignored.

     */
    void parse_cl();

protected:

    /*! Computes velocity dispersion projection statistics for all particle
        types specified in types_string[]. The char array, types_string[],
        contains up to six non-whitespace characters, which correspond to the
        six particle types. When a '1' appears in types_string[], it indicates
        that the user wishes to include particles of the corresponding type.
        The types are:

            0: gas / SPH particles
            1: dark matter particles
            2: disk particles
            3: bulge particles
            4: new star particles
            5: black hole particles

        For example, to specify all stellar particles, we would set

        \code
        char types_string[] = '001110'
        \endcode

        The velocity dispersions will be measured using the slit-based method.
        By default, the code will report the mean, maximum, minimum, and
        standard deviation of \f$\sigma\f$ along 1000 randomly selected lines of
        sight passing through the center of mass of the types of particles
        specified in types_string[]. Setting nucleus_centered = true will
        cause the measurements of \f$\sigma\f$ to be centered on the position of
        the black hole specified by which_bh; setting which_bh = 0 centers the
        measurements on the first black hole in the snapshot file, which_bh = 1
        centers the measurement on the second and so on.

        Setting age_cut = 0 causes the function to include all stars
        appearing in the slit (mask) in the calculation of \f$\sigma\f$. Setting
        age_cut > 0 computes the velocity dispersion of stars older than
        age_cut time units while setting age_cut < 0 computes the velocity
        dispersion of stars younger than age_cut time units. */
    void proj_stats();

    /*! if `--view particles` was specified, view() creates an image of
        the snapshot along the z-axis and saves the image to disk as a PNG file.

        If `--view interactive`, view() launches the interactive snapshot
        viewer, which allows the user to rotate and zoom the snapshot file and
        change the gamma value.

        If `--view gas`, view() renders an image of the gas component
        of a snapshot using a volume rendering scheme.

        If `--view flux`, view() renders an image of the stellar component
        using a volume rendering scheme. */
    void view();

    /*! Parses the --view command (flux, gas, particles, or interactive). */
    void parse_view(const char* view_method);

    /*! Interpolates between snapshots using a third-order interpolation scheme.
        Only the positions of particles are interpolated. This requires two
        snapshots to be specified. The default number of interpolated snapshots
        is N_inter=1 (one). Specifying a larger value of N_inter, creates more
        output files. The interpolated files are evenly spaced in time between
        the two endpoint files.*/
    void interp();

    /*! Shifts the center of the snapshot to cli_actions::shift[], or the center
        of mass, or the position of a black hole particle. The resulting system
        has zero net momentum.*/
    void shift_snapshot_center(particle_type_group& _particles);

    /*! Performs modifications to the snapshot file and save the modified file to
        the disk. */
    void modify_snapshot();

    /*! Parse the argument of the `--modify` command.*/
    void parse_modify(const char* mod);

    /*! Displays basic information about the snapshot. */
    void info() const;

    /*! Sets the center point of the system to the values specified by `-c # # #`
    */
    void set_center(const uint c);

    void set_types(const uint tps)
    {
        for (uint i = 0; i < 6; ++i) { types_string[i] = argv[tps][i]; }
    }

    /*! Performs the task specified on the command line.*/
    void run();

    /*! Interprets the particle types specification string provided in the
        six-character command line argument.  */
    void interpret_types();

    /*! Tests whether the argument string is a command line (short) flag.
        Returns true if the flag is short and false otherwise. */
    bool is_char_flag(const char* str) const;

    /*! Tests whether the argument string is a command line (long) flag.
        Returns true if the flag is long and false otherwise. */
    bool is_long_flag(const char* str) const;

    /*! Reports errors. If b is true, str is used as part of an error message
        and execution is terminated. */
    void arg_error(const bool b, const char* str) const;

    /*! Performs bounds-checking on the argv array in order to prevent
        segmentation faults. */
    char* check_bounds(const uint n) const;

    /*! Interprets `--long_flags`. */
    uint interpret_long_flags(const uint arg_n);

    /*! Interprets short, single character flags (like `-s`). */
    uint interpret_char_flags(const uint arg_number);

    /*! Sets up an associative array mapping long command flags (strings) to
        the long_flags enumeration entries. */
    void make_flag_map();

    /*! * \brief Executes an ECMAScript (technically, a QtScript)
     *
     * \param _types specifies the default types of particles that will be
     * available to the script. The script itself can also specify which
     * particle types to use.
     * \param _filename is the file name of the script file.
     */
    void execute_script(types _types, char* _filename);
};

#endif // CLI_ACTIONS_H
