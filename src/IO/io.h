/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
    \brief Provides snapshot file I/O classes and function prototypes.

    \todo Add code for reading other snapshot formats.
*/

#ifndef IO_H
#define IO_H

#include "Core/global.h"
#include "Core/basic_particle.h"
#include "IO/gadget.h"
//headers for new format input/output routines go here.

#include <QVector>


/*! \brief Provides snapshot file I/O classes and function prototypes.
*/
struct snapshot
{
private:

    char*       file_name      {nullptr};
    snap_format current_format {snap_format::none};

public:

    generic_header  header;

    vmeta_particle  data;

    /*! Use this for deferred initialization. */
    snapshot() {}

    /*! This constructor immediately opens and loads a snapshot file. */
    snapshot(char* fname) : file_name(fname) { open(fname); }

    /*! Opens the snapshot named fname, determines the formatting of the file,
        then uses the appropriate format reader to load the file's data.*/
    void open(char* fname);

    /*! Saves the current snapshot data in a specified format to a file named
        fname. */
    void save_as(char* fname, snap_format format = snap_format::Gadget);

    /*! Saves the current snapshot date in current_format to a file_name. */
    void save();

    /*! Returns the value of current_format. */
    snap_format get_current_format() { return current_format; }

    /*! Determines the format of the snapshot file and checks to see whether the
        file format is valid (i.e., self-consistent and uncorrupted). Returns
        snap_format::none if the file is not a valid GSnap-readable format. */
    static snap_format validate_format(char* fname);

private:

    /*! Determines the format of the snapshot file. This differs from the public
        member validate_format. This method merely determines the general format
        of the file. It does not determine the specific sub-format and it does
        not check whether the entire file is correctly formatted. */
    static snap_format deduce_format(const char* fname);
};

#endif

