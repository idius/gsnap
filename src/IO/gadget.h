/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
    \brief Provides GADGET-2 type 1 file I/O classes and function prototypes.
*/

#ifndef GADGET_H
#define GADGET_H

#include "Core/global.h"
#include "IO/basic_io.h"

#include <fstream>
#include <vector>
#include <iostream>
#include <QString>


/// The header for the standard GADGET-2, type 1 file format.
struct gadget_header
{
    /*! The number of particles of each type in this file */
    int      npart[6];

    /*! The mass of particles of each type. If 0, then the masses  are
        explicitly stored in the mass-block of the snapshot file (otherwise they
        are omitted) */
    double   mass[6];

    /*! The time of snapshot file in simulation units*/
    double   time;

    /*! The redshift of the snapshot.*/
    double   redshift;

    /*! Flags whether or not the simulation included star formation.*/
    int      flag_sfr;

    /*! Flags whether feedback was included or not.*/
    int      flag_feedback;

    /*! The total number of particles of each type in this snapshot. This can be
        different from npart if one is dealing with a multi-file snapshot.*/
    int      npartTotal[6];

    /*! Flags whether cooling was included.*/
    int      flag_cooling;

    /*! Specifies the number of files in multi-file snapshot.*/
    int      num_files;

    /*! The box-size of a cosmological simulation in kpc.*/
    double   BoxSize;

    /*! Matter density in units of critical density, \f$\Omega_0\f$*/
    double   Omega0;

    /*!  The cosmological constant parameter, \f$\Omega_\Lambda\f$*/
    double   OmegaLambda;

    /*! The Hubble parameter in units of 100 km/sec/Mpc, \f$h\f$*/
    double   HubbleParam;

    /*! Flags whether the file contains formation times of star particles.*/
    int      flag_stellarage;

    /*! Flags whether the file contains metallicity values for gas and star
        particles*/
    int      flag_metals;

    /*! High word of the total number of particles of each type.*/
    uint     npartTotalHighWord[6];

    /*! Flags whether the IC-file contains entropy instead of internal
        energy, u */
    int      flag_entropy_instead_u;

    /*! Extra filler space. Nothing is stored here. This simply ensures that
        the header is exactly 256 Bytes */
    char     fill[60];
};


/*! \brief A container for storing an individual GADGET snapshot file.

   Stores all GADGET snapshot data in a format that resembles the native
   GADGET-2 file format, meaning that positions, particle ID numbers, masses,
   velocities, etc. are all stored as separate arrays.*/
class gadget_io
{
protected:

    bool        initialized;
    snap_format file_fmt;

public:

    gadget_header   header;
    int         Ntot;
    int         Ngas;
    int         Ndm;
    int         Ndisk;
    int         Nbulge;
    int         Nnew;
    int         Nbh;
    float       Mtot;
    float       Mdm;
    vfloat      pos;
    vfloat      vel;
    vfloat      m;
    vint        id;
    vfloat      u;
    vfloat      rho;
    vfloat      ne;
    vfloat      nh;
    vfloat      hsml;
    vfloat      sfr;
    vfloat      age;
    vfloat      metz;
    vfloat      pot;
    vfloat      Mbh;
    vfloat      Mdot_bh;

    gadget_io() { initialized = false; }

    gadget_io(char* fname) { load(fname); }

    ~gadget_io() { clear_all(); }

    /*! Clear all of the vectors contained in the galaxy_data structure. */
    void clear_vecs();

    void clear_all();

    /*! Check that the input file is in the correct format and that it has not
        been corrupted.  We do this by first testing whether the file begins
        with the integer 256. We then make sure that there are at least 12
        properly-formatted blocks of data in the file. Returns the specific
        format of the gadget snapshot and snap_format::none if the format is
        not recognized.

        \todo Make the check_format function perform a more rigorous test.*/
    static snap_format check_format(char* fname);

    /*! Overloaded form of check_format(char* fname.)*/
    static snap_format check_format(QString qfname);

    /*! Load a GADGET type 1 snapshot from the file *fname */
    void load(char* fname);

    /*! Load a GADGET type 1 snapshot from the file qfname */
    void load(QString qfname);

    /*! Save the snapshot to the file *fname in GADGET type 1 format.*/
    void save_as(char* fname);

    /*! Save the snapshot to the file qfname in GADGET type 1 format.*/
    void save_as(QString qfname);

    /*! Recomputes header fields based upon the current contents of the
        snapshot.*/
    void update_header();

    /*! Sets the snapshot format of the output file. */
    void set_file_format(snap_format fmt) { file_fmt = fmt; }

    /*! Get the current snapshot format. If this is called immediately after a
        snapshot file is loaded, this specifies the format of the input file
        (i.e., the result of running check_format() on the snapshot). This also
        indicates the format in which the snapshot will be saved if
        save_output() is called. */
    snap_format get_file_format() { return file_fmt; }

    /*! Transfers data from the input GADGET snapshot file (the data stored in
        an object of this class) to a more generic snapshot object. */
    void to_snapshot(generic_header& head, vmeta_particle& data)
    {
        gadget_to_snapshot(head);
        gadget_to_snapshot(data);
    }

    /*! Transfers data from the generic snapshot format to the gadget_io object
        to prepare the data for storage on the disk. */
    void from_snapshot(generic_header& head, vmeta_particle& data)
    {
        snapshot_to_gadget(head);
        snapshot_to_gadget(data);
        initialized = true;
    }

private:

    void gadget_to_snapshot(generic_header& head);

    void gadget_to_snapshot(vmeta_particle& data);

    void snapshot_to_gadget(generic_header& head);

    void snapshot_to_gadget(vmeta_particle& data);
};

#endif
