/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
    \brief Provides general (i.e., format-agnostic) IO routines.
*/

#ifndef BASIC_IO_H
#define BASIC_IO_H

#include "Core/global.h"
#include "Core/basic_particle.h"

#include <fstream>
#include <vector>
#include <iostream>
#include <QVector>


typedef std::vector<float> vfloat;

typedef std::vector<int> vint;

typedef QVector<meta_particle> vmeta_particle;


struct generic_header
{
    /*! The total number of particles in the snapshot. */
    uint   n_total;

    /*! The number of particles of the 6 particle species in the pcat enum. */
    uint   n_category[6];

    /*! The softening length of the 6 particle species in the pcat enum. */
    kpc    softening[6];

    /*! The mass of particles of each type. If 0, then the masses  are
        explicitly stored in the mass-block of the snapshot file (otherwise they
        are omitted) */
    double   mass[6];

    /*! The time of snapshot file in simulation units*/
    double   time;

    /*! The redshift of the snapshot.*/
    double   redshift;

    /*! Flags whether or not the simulation included star formation.*/
    int      flag_sfr;

    /*! Flags whether feedback was included or not.*/
    int      flag_feedback;

    /*! The total number of particles of each type in this snapshot. This can be
        different from npart if one is dealing with a multi-file snapshot.*/
    int      npartTotal[6];

    /*! Flags whether cooling was included.*/
    int      flag_cooling;

    /*! Specifies the number of files in multi-file snapshot.*/
    int      num_files;

    /*! The box-size of a cosmological simulation in kpc.*/
    double   BoxSize;

    /*! Matter density in units of critical density, \f$\Omega_0\f$*/
    double   Omega0;

    /*!  The cosmological constant parameter, \f$\Omega_\Lambda\f$*/
    double   OmegaLambda;

    /*! The Hubble parameter in units of 100 km/sec/Mpc, \f$h\f$*/
    double   HubbleParam;

    /*! Flags whether the file contains formation times of star particles.*/
    int      flag_stellarage;

    /*! Flags whether the file contains metallicity values for gas and star
        particles*/
    int      flag_metals;

    /*! High word of the total number of particles of each type.*/
    uint     npartTotalHighWord[6];

    /*! Flags whether the IC-file contains entropy instead of internal
        energy, u */
    int      flag_entropy_instead_u;
};

#endif

