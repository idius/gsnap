/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
    \brief Provides the Preview window GUI.

    The Qt make system uses this header, along with the preview.ui user
    interface specification file, to generate a source file called
    moc_gui_preview.cpp which can then be compiled to create a Qt GUI window.

\todo Add more interactivity features to the preview window. for instance, allow
      the user to use the mouse to click and drag the image. this will require
      the introduction of a "mode" selector so that the movements of the mouse
      will be interpreted correctly.

\todo Allow the user to switch between perspective projection and orthographic
      projection.

\todo Allow the user to render the current scene using the volume rendering from
      within this window and load the result and / or save it to a file.
*/

#include "GUI/gui_preview.h"

#include <omp.h>


void gui_preview::contextMenuEvent(QContextMenuEvent* event)
{
    bool sig = (gmw_pointer_set && length > 0 && width > 0 && show_slit);
    QMenu menu(this);
    menu.addAction(show_infoAction);
    menu.addAction(rotate_to_zAction);
    menu.addAction(centerAction);

    if (sig) { menu.addAction(compute_sigmaAction); }

    menu.exec(event->globalPos());
}


void gui_preview::mousePressEvent(QMouseEvent* event)
{
    if (event->button() == Qt::LeftButton)
    {
        focus_point = event->pos();

        center_point = focus_point - viewLabel->pos();

        center_here();
    }
}


void gui_preview::set_gmw_pointer(gui_main_window* _gmw)
{
    gmw_pointer = _gmw;
    gmw_pointer_set = true;

    //create actions and set connections that depend on the main window.

    compute_sigmaAction = new QAction(tr("Compute &sigma"), this);
    compute_sigmaAction->setShortcut(tr("s"));
    compute_sigmaAction->setStatusTip(tr("Compute sigma within the slit"));
    connect(compute_sigmaAction, SIGNAL(triggered()), this,
            SLOT(compute_sigma()));
    addAction(compute_sigmaAction);

    connect(this, SIGNAL(main_compute_sigma()), (QObject*)gmw_pointer,
            SLOT(compute_sigma()));
}


void gui_preview::update()
{
    if (rotated) { particles->set_direction(theta, phi); }

    static pixel_array pgroup_image(resolution, resolution, view_size);

    pgroup_image.set_view_size(view_size);
    pgroup_image.fill(0);
    particles->rect_mesh(&pgroup_image);
    pgroup_image.rescale_value(gamma);

    static QImage gal_image(resolution, resolution, QImage::Format_RGB32);

    gal_image.fill(0);

    uint active_pix_color;

    for (size_t j = 0; j < resolution; ++j)
    {
        for (size_t i = 0; i < resolution; ++i)
        {
            float grayscale = pgroup_image.get_pixel(i, j);

            if (grayscale)
            {
                grayscale *= 255;
                active_pix_color = qRgb( grayscale,
                                         grayscale,
                                         grayscale );

                gal_image.setPixel(i, j, active_pix_color );
            }
        }
    }

    // Display the slit
    if (show_slit) { draw_slit(&gal_image); }

    // Display the slider info
    if (show_info) { draw_info(&gal_image); }

    // Save image to disk
    if (image) { gal_image.save(image_name); }

    // (Re)draw the preview window
    viewLabel->setPixmap(QPixmap::fromImage(gal_image, 0));
}


void gui_preview::update_gamma(int g)
{
    gamma = 0.001 * g;

    adjust_g = true;

    update();

    adjust_g = false;
}


void gui_preview::update_theta(int t)
{
    theta = 0.01 * t;

    rotated = true;

    update();

    rotated = false;
}


void  gui_preview::update_phi(int p)
{
    phi = 0.01 * p;

    rotated = true;

    update();

    rotated = false;
}


void  gui_preview::update_view_size(int b)
{
    view_size = 0.1 + 0.00001 * b * b;

    zoomed = true;

    update();

    zoomed = false;
}


bool  gui_preview::save_image()
{
    image_name = QFileDialog::getSaveFileName(0, tr("Save Image"), "untitled.png",
                 tr("Images (*.png *.jpg)") );

    if (image_name == "")
    {
        QString message = "no image file name selected\n";
        std::cerr << message.toStdString();
        return false;
    }

    if ( !image_name.endsWith(".png") && !image_name.endsWith(".PNG") && \
            !image_name.endsWith(".jpg") && !image_name.endsWith(".JPG") )
        { image_name = image_name + ".png"; }

    image = true;

    update();

    image = false;

    return true;
}


void gui_preview::display_slit(bool ss)
{
    show_slit = ss;
    update();
}


void gui_preview::display_info(bool si)
{
    show_info = si;
    update();
}


void gui_preview::set_slit(float _width, float _length)
{
    width = _width;
    length = _length;
}


void gui_preview::draw_slit(QImage* image)
{
    if (gmw_pointer_set)
    {
        float ip_size = ((float)resolution) / view_size;

        slit_width = width * ip_size;
        slit_length = length * ip_size;

        // the pixel coordinates of the upper left corner are (slit_x, slit_y)
        slit_x = 0.5 * ((int)resolution - slit_width);
        slit_y = 0.5 * ((int)resolution - slit_length);

        QPainter p;
        QPen pen;
        QColor line_color(0, 100, 255, 75);
        pen.setColor(line_color);
        pen.setWidth(2);

        if (slit_width > 0 && slit_length > 0)
        {
            p.begin(image);
            p.setRenderHint(QPainter::Antialiasing);
            p.setPen(pen);
            p.drawRect(slit_x, slit_y, slit_width, slit_length);
            p.end();
        }
    }
}


void gui_preview::draw_info(QImage* image)
{
    QFont font;
    QPen pen;
    QColor font_color(200, 200, 255, 160);
    pen.setColor(font_color);
    font.setPointSize(10);

    QString info_string("theta = ");
    info_string += QString::number(theta, 'f', 3);
    info_string += "    ";
    info_string += "phi = ";
    info_string += QString::number(phi, 'f', 3);
    info_string += "    ";
    info_string += "view size = ";
    info_string += QString::number(view_size, 'f', 4);
    info_string += " kpc";

    QPainter p;
    p.begin(image);
    p.setRenderHint(QPainter::Antialiasing);
    p.setPen(pen);
    p.setFont(font);
    p.drawText(5, 15, info_string);
    p.end();
}


void gui_preview::rotate_to_z()
{
    particles->rotate(theta, phi);
    emit zero_sliders(0);
}


void gui_preview::compute_sigma()
{
    if (gmw_pointer_set && length > 0 && width > 0 && show_slit)
    {
        rotate_to_z();
        emit main_compute_sigma();
    }
}


void gui_preview::center()
{
    float cm[7];
    particles->compute_cm(cm);
    particles->shift(cm);
    update();
}


void gui_preview::center_here()
{
    rotate_to_z();

    float psize = view_size / ((float)resolution);

    float cm[7];
    cm[0] = 0;
    cm[1] = -0.5 * view_size + (float)center_point.x() * psize;
    cm[2] = 0.5 * view_size - (float)center_point.y() * psize;
    cm[3] = 0;
    cm[4] = 0;
    cm[5] = 0;
    cm[6] = 0;

    if (center_point.x() > 0 && center_point.y() > 0)
    {
        particles->shift(cm);
        update();
    }
}
