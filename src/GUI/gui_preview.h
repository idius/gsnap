/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
    \brief Provides the Preview window GUI.

    The Qt Make system uses this header, along with the preview.ui user
    interface specification file, to generate a source file called
    moc_gui_preview.cpp which can then be compiled to create a Qt GUI window.

\todo Add more interactivity features to the preview window. for instance, allow
      the user to use the mouse to click and drag the image. this will require
      the introduction of a "mode" selector so that the movements of the mouse
      will be interpreted correctly.

\todo Allow the user to switch between perspective projection and orthographic
      projection.

\todo Allow the user to render the current scene using the volume rendering from
      within this window and load the result and / or save it to a file.
*/


#ifndef GUI_PREVIEW_H
#define GUI_PREVIEW_H


#include "ui_preview.h"
#include "Viz/image.h"
#include "Viz/pixel_array.h"
#include "Core/global.h"
#include "IO/io.h"

#include <cstdlib>
#include <QDialog>
#include <QFileDialog>
#include <QPainter>
#include <QPen>
#include <QFont>
#include <QColor>
#include <QAction>
#include <QMenu>
#include <QContextMenuEvent>
#include <QKeySequence>
#include <QPoint>
#include <QWidget>


class gui_main_window;


/*! \brief Creates the "Preview" window.

   Provides the interactive preview window for quickly viewing and manipulating
   snapshots. The user can rotate, zoom, center, compute sigma, show or hide
   the viewing info, adjust gamma, and save an image of the current preview
   window contents. */
class gui_preview : public QDialog, public Ui_Preview
{
    Q_OBJECT

protected:

    gui_main_window* gmw_pointer;

    particle_type_group* particles;
    QString         file_name;
    QString         image_name;
    QAction*        show_infoAction;
    QAction*        rotate_to_zAction;
    QAction*        compute_sigmaAction;
    QAction*        centerAction;
    QColor          color;
    QPoint          focus_point;
    QPoint          center_point;
    float           gamma = 0.35;
    float           view_size = 100; // measured in kiloparsecs
    float           theta = 0;
    float           phi = 0;
    float           width = 0;
    float           length = 0;
    uint            resolution = 550; // measured in pixels
    int             slit_x;
    int             slit_y;
    int             slit_width;
    int             slit_length;
    bool            gmw_pointer_set;
    bool            zoomed = true;
    bool            rotated = true;
    bool            adjust_g = true;
    bool            resized = true;
    bool            image;
    bool            show_slit = true;
    bool            show_info = true;

public:

    /*! The gui_preview constructor. */
    gui_preview(particle_type_group* _particles, QWidget* parent_wdgt)
        : QDialog(parent_wdgt)
    {
        particles = _particles;
        setupUi(this);
        make_actions();
        set_connections();
        update();
    }

    /*! the gui_preview destructor */
    ~gui_preview() { emit closed(); }

    /*! Set the gui_main_window pointer (gmw_pointer) to _gmw */
    void  set_gmw_pointer(gui_main_window* _gmw);

    /*! Set the slit dimensions */
    void  set_slit(float width, float length);

    /*! Perform the drawing tasks needed to show the diffraction slit. */
    void  draw_slit(QImage* image);

    /*! Perform the drawing tasks needed to show the slider information. */
    void  draw_info(QImage* image);

    void mousePressEvent(QMouseEvent* event);

public slots:

    /*! Do whatever is needed to update the image and then display it in the
        window using the QLable object, viewLabel */
    void  update();

    /*! View the system from the theta = 0.01*t direction */
    void  update_theta(int t);

    /*! View the system from the phi = 0.01*p direction */
    void  update_phi(int p);

    /*! Update the scaling exponent, gamma, then display the updated image */
    void  update_gamma(int g);

    /*! Zoom the image by changing the size of the physical viewing box,
        view_size. Here, view_size = 1 + 0.0001*b*b, where b is the integer
        value of the zoom slider */
    void  update_view_size(int b);

    /*! Show / hide the slit. It appears as a rectangle in the preview window.*/
    void  display_slit(bool ss);

    /*! Show / hide rotation angle and view size in the preview window. */
    void  display_info(bool si);

    /*! Open a file chooser dialog to select a file name, then tell the update
        function to save the current preview image to disk as a PNG or JPEG
        file. */
    bool  save_image();

    /*! Rotate the system such that the current viewing direction becomes the
        new z-axis */
    void  rotate_to_z();

    /*! Compute the velocity dispersion of the stars within the slit */
    void  compute_sigma();

    /*! Center the preview on center of mass.*/
    void  center();

    /*! Center the preview on the place where the cursor was pointing when the
        left mouse button was clicked (i.e., center_point) */
    void  center_here();

signals:

    /*! Informs the main window that the preview window is closed*/
    void closed();

    /*! Sets both direction sliders to angle. The angle should always zero for
        this signal, hence the name. */
    void zero_sliders(int angle);

    /*! Tells the main window to compute sigma and display the results. */
    void main_compute_sigma();

protected:

    /*! Set up the context menu for the preview window */
    void contextMenuEvent(QContextMenuEvent* event);

    /*! Set the connections needed to make the GUI work properly */
    void set_connections()
    {
        connect(gammaSlider, SIGNAL(valueChanged(int)), this,
                SLOT(update_gamma(int)));

        connect(thetaSlider, SIGNAL(valueChanged(int)), this,
                SLOT(update_theta(int)));

        connect(phiSlider, SIGNAL(valueChanged(int)), this,
                SLOT(update_phi(int)));

        connect(box_sizeSlider, SIGNAL(valueChanged(int)), this,
                SLOT(update_view_size(int)));

        connect(save_imageButton, SIGNAL(clicked()), this,
                SLOT(save_image()));

        connect(this, SIGNAL(zero_sliders(int)), thetaSlider,
                SLOT(setValue(int)));

        connect(this, SIGNAL(zero_sliders(int)), phiSlider,
                SLOT(setValue(int)));
    }

    /*! Set up the actions, and make the appropriate action connections. */
    void make_actions()
    {
        show_infoAction = new QAction(tr("Show &Info"), this);
        show_infoAction->setCheckable(true);
        show_infoAction->setShortcut(tr("Ctrl+I"));
        show_infoAction->setStatusTip(tr("Show the values of the sliders."));
        show_infoAction->setChecked(true);
        connect(show_infoAction, SIGNAL(toggled(bool)), this,
                SLOT(display_info(bool)));
        addAction(show_infoAction);


        rotate_to_zAction = new QAction(tr("Make &z-axis"), this);
        rotate_to_zAction->setShortcut(tr("Ctrl+Z"));
        rotate_to_zAction->setStatusTip(tr("Make this directon the new z_axis"));
        connect(rotate_to_zAction, SIGNAL(triggered()), this,
                SLOT(rotate_to_z()));
        addAction(rotate_to_zAction);


        centerAction = new QAction(tr("&Center"), this);
        centerAction->setShortcut(tr("Ctrl+C"));
        centerAction->setStatusTip(tr("Center the system"));
        connect(centerAction, SIGNAL(triggered()), this,
                SLOT(center()));
        addAction(centerAction);
    }
};

#endif
