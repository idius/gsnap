/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
    \brief Provides the Main Application window GUI.
*/


#include "GUI/gui_main.h"

#pragma GCC optimize ("Os")

/*! open a file chooser dialog to choose a file name, then open the snapshot.*/
int gui_main_window::open_snapshot()
{
    file_name = QFileDialog::getOpenFileName(0, tr("Open Snapshot"));

    if (file_name == "")
    {
        textLog->append("<font color=\"red\">No file was selected!</font>");
        return 0;
    }

    // close previously-opened file
    close();

    //test whether the file is a valid GADGET snapshot.
    if (gadget_io::check_format(file_name) == snap_format::none)
    {
        textLog->append("<font color=\"red\"> The file does not appear to be "
                        " valid.</font>");
        return 1;
    }

    // This has to be static or else bad things will happen! Some more work
    // needs to be done in order to clean this situation up.
    static gadget_io _gal;

    gal = &_gal;

    gal->load(file_name);

    my_particles->init(my_types, gal);

    connections_set = true;

    output_file += file_name + ".edit";

    emit f_center(file_name);

    textLog->append("opened " + file_name);

    return 0;
}


/*! Close the current snapshot file; free the memory */
void gui_main_window::close()
{
    if (connections_set)
    {
        gal->clear_vecs();
        my_particles->free();
        connections_set = false;

        if (preview_window_open)
        {
            preview_pointer->done(0);
            preview_window_open = false;
        }
    }
}


/*! Save the modified snapshot to disk. */
void gui_main_window::save_snapshot()
{
    if (connections_set)
    {
        gal->save_as(output_file);

        textLog->append("snapshot saved as " + output_file);
    }
}


/*! Open a file chooser dialog, choose a file name, save snapshot to disk. */
int gui_main_window::save_as()
{
    output_file = QFileDialog::getSaveFileName(0, tr("Save Snapshot..."));

    if (output_file == "")
    {
        textLog->append("<font color=\"red\">No file was selected!</font>");
        return 0;
    }

    save_snapshot();

    return 0;
}


/*! Update the gal data structure, but do not save to disk. Ultimately, we may
also need to update the header if particles are removed or added. */
void gui_main_window::update()
{
    if (connections_set)
    {
        my_particles->update_gal();

        textLog->append("updated the gal data structure.");
    }
}


void gui_main_window::preview()
{
    if (connections_set && !preview_window_open)
    {
        gui_preview preview_window(my_particles, this);

        preview_pointer = &preview_window;

        preview_window_open = true;

        preview_pointer->set_gmw_pointer(this);

        preview_window.show();

        preview_window.exec();

        preview_window_open = false;
    }
    else if (connections_set && preview_window_open)
    {
        preview_pointer->set_slit(width, length);
        preview_pointer->update();
    }
}


void gui_main_window::preview_closed()
{
    preview_window_open = false;
}


/*! Set the x-shift (-x displacement) */
void gui_main_window::set_x_shift(double x)
{
    x_shift = x;
}


/*! Set the y-shift (-y displacement) */
void gui_main_window::set_y_shift(double y)
{
    y_shift = y;
}


/*! Set the z-shift (-z displacement) */
void gui_main_window::set_z_shift(double z)
{
    z_shift = z;
}


/*! Set the theta value */
void gui_main_window::set_theta(double t)
{
    theta = t;
}


/*! Set the phi value */
void gui_main_window::set_phi(double ph)
{
    phi = ph;
}


/*! Set the slit position angle, alpha */
void gui_main_window::set_alpha(double a)
{
    alpha = a;
}


/*! Set the slit length */
void gui_main_window::set_length(double l)
{
    length = l;

    if (auto_update && width * length)
    {
        set_slit_dims();
        compute_sigma();
    }
}


/*! Set the slit width */
void gui_main_window::set_width(double w)
{
    width = w;

    if (auto_update && width * length)
    {
        set_slit_dims();
        compute_sigma();
    }
}


/*! Shift the positions of particles by -x-shift, -y-shift, -z-shift */
void gui_main_window::shift()
{
    if (connections_set)
    {
        center_of_mass[1] = x_shift;
        center_of_mass[2] = y_shift;
        center_of_mass[3] = z_shift;

        my_particles->shift(center_of_mass);

        x_shift = 0;
        y_shift = 0;
        z_shift = 0;
        emit f_shift(0);

        textLog->append("The system has been shifted by ["
                        + QString::number(center_of_mass[1]) + ", "
                        + QString::number(center_of_mass[2]) + ", "
                        + QString::number(center_of_mass[3]) + "]."
                        );

        if (auto_update && width * length)
            { compute_sigma(); }

        if (preview_window_open)
            { preview_pointer->update(); }
    }
}


/*! Rotate the system so that the new z-axis points along the current
(theta, phi) direction */
void gui_main_window::rotate()
{
    if (connections_set)
    {
        my_particles->rotate(rad(theta), rad(phi));

        textLog->append("The system has been rotated by ("
                        + QString::number(theta) + ", "
                        + QString::number(phi) + ")."
                        );

        theta = 0;
        phi = 0;
        emit f_rotate(0);

        if (auto_update && width * length)
            { compute_sigma(); }

        if (preview_window_open)
            { preview_pointer->update(); }
    }
}


/*! Set up the slit */
void gui_main_window::set_slit_dims()
{
    if (connections_set)
    {
        my_particles->set_slit(float(width), float(length), float(alpha));

        textLog->append("The slit has been set: l = " + QString::number(length)
                        + ", w = " + QString::number(width) + ", "
                        + "a = " + QString::number(alpha)
                        );

        alpha = 0;
        emit f_set_alpha(0);

        if (connections_set && preview_window_open)
        {
            preview_pointer->set_slit(width, length);
            preview_pointer->update();
        }

        if (auto_update && width * length)
            { compute_sigma(); }
    }
}


/*! Compute the velocity dispersion of stars appearing within the slit. */
void gui_main_window::compute_sigma()
{
    if (connections_set && width * length)
    {
        sigma = my_particles->sigma();

        // be sure to set alpha = 0 in my_particles to prevent double-click
        //errors
        my_particles->set_slit((float) width, (float) length, 0);

        QString sigma_str = QString::number(sigma);
        sigma_str.truncate(7);
        sigma_str = sigma_str + " km/s";

        QString n_slit = QString::number(my_particles->get_number_in_slit());

        textLog->append("sigma = " + sigma_str + "\nn_slit = "+ n_slit);

        emit f_sigma(sigma_str);
        emit f_n_slit(n_slit);

        if (preview_window_open) { preview_pointer->update(); }
    }
}


/*! Reset the values entered into the GUI */
void gui_main_window::zero()
{
    auto_update = false;
    emit f_zero_au(false);
    emit f_zero(0);
}


/*! activate or disable the auto-update button */
void gui_main_window::set_auto_update(bool val)
{
    auto_update = val;
}


/*! Compute the center of mass, then shift the coordinates so that the center
of mass is at the origin 0,0,0 */
void gui_main_window::center()
{
    if (connections_set)
    {
        my_particles->compute_cm(center_of_mass);
        my_particles->shift(center_of_mass);

        emit f_center("Centering was successful!");

        textLog->append("Centering was successful!\n The old center-of-mass "
                        "coordinates were:\n( "
                        + QString::number(center_of_mass[1]) + ", "
                        + QString::number(center_of_mass[2]) + ", "
                        + QString::number(center_of_mass[3]) + " )"
                        );

        if (preview_window_open) { preview_pointer->update(); }
    }
}


/*! Show the type selection window. */
void gui_main_window::select_types()
{
    selector selection_window(&my_types, this);

    selection_window.show();

    selection_window.exec();
}


/*! Show the About window. */
void gui_main_window::show_about()
{
    gui_about about_window(this);

    about_window.show();

    about_window.exec();
}


/*! Show the About window. */
void gui_main_window::show_help_browser()
{
    gui_help_browser help_browser_window(this);

    help_browser_window.show();

    help_browser_window.exec();
}

