/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
    \brief Provides the Help Browser window GUI.

    The  Qt make system uses this header, along with the help_browser.ui user
    interface specification file, to generate a source file called
    moc_gui_help_browser.cpp which can be compiled to create a Qt GUI window.

   \todo   Test for and handle the case in which no help files exist.
*/

#ifndef INC_GUI_HELP_BROWSER_H
#define INC_GUI_HELP_BROWSER_H

#include "Core/global.h"
#include "ui_help_browser.h"

#include <QtGui>
#include <QObject>
#include <QDialog>
#include <QWidget>
#include <QPainter>
#include <QFile>
#include <QStringList>


/*! \brief Creates the "Help Browser" window.

    Provides the Webkit browser window accessed by clicking Help->Using GSnap in
    the main application window. */
class gui_help_browser : public QDialog, public Ui_help_browser
{
    Q_OBJECT

public:

    gui_help_browser(QWidget* parent) : QDialog(parent)
    {
        setupUi(this);

        webView->setRenderHint(QPainter::TextAntialiasing);

        // Create a list of possible file locations
        QStringList files;
        files << QString::fromUtf8("HTML/html/Home.html");
        files << QString::fromUtf8("Home.html");
        files << QString::fromUtf8("~/.gsnap/html/Home.html");
        files << QString::fromUtf8("/usr/share/doc/gsnap/Home.html");
        files << QString::fromUtf8("/usr/local/share/doc/gsnap/Home.html");

        // Iterate through the list of possible file locations
        for (int i = 0; i < files.size(); i++)
        {
            QFile helpfile(files.at(i));

            if (helpfile.exists())
            {
                webView->setUrl(QUrl(files.at(i)));
                break;
            }
        }

        // Test for and handle the case in which no file locations exist.
    }
};

#endif

