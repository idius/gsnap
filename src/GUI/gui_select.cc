/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
   \brief Member function definitions for the selector class.
*/

#include "gui_select.h"


selector::selector(types* _my_types, QWidget* parent) : QDialog(parent)
{
    my_types = _my_types;

    setupUi(this);

    set_connections();
}


void selector::set_connections()
{
    connect(Gas_CheckBox, SIGNAL(toggled(bool)), this,
            SLOT(Gas(bool)));

    connect(DM_CheckBox, SIGNAL(toggled(bool)), this,
            SLOT(DM(bool)));

    connect(Disk_CheckBox, SIGNAL(toggled(bool)), this,
            SLOT(Disk(bool)));

    connect(Bulge_CheckBox, SIGNAL(toggled(bool)), this,
            SLOT(Bulge(bool)));

    connect(New_stars_CheckBox, SIGNAL(toggled(bool)), this,
            SLOT(New_stars(bool)));

    connect(BH_CheckBox, SIGNAL(toggled(bool)), this,
            SLOT(BH(bool)));
}
