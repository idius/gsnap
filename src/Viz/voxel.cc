/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
    \brief Member functions for the voxel class.
*/



#include "voxel.h"

voxel::voxel()
{
    zero();
}


voxel::voxel(const voxel& s)
{
    mass = s.mass;
    blue = s.blue;
    red = s.red;
    //v = s.v;
    u = s.u;
    //met = s.met;
}


voxel voxel::operator=(const voxel& s)
{
    mass = s.mass;
    blue = s.blue;
    red = s.red;
    //v = s.v;
    u = s.u;
    //met = s.met;

    return *this;
}


voxel voxel::operator+(const voxel& s)
{
    voxel temp;

    const float total_mass = mass + s.mass;

    if (total_mass > 0)
    {
        const float itotal_mass = 1.0 / total_mass;

        temp.mass = mass + s.mass;
        temp.blue = blue + s.blue;
        temp.red = red + s.red;

        temp.u = (u * mass + s.u * s.mass) * itotal_mass;
        //temp.met = (met * mass + s.met * s.mass) * itotal_mass;
    }
    else
    {
        temp.mass = mass;
        temp.blue = blue + s.blue;
        temp.red = red + s.red;
        temp.u = u;
        //temp.met = met;
    }

    return temp;
}


voxel voxel::operator+=(const voxel& s)
{
    const float total_mass = mass + s.mass;

    if (total_mass > 0)
    {
        const float itotal_mass = 1.0 / total_mass;

        const float _mass = mass;

        mass += s.mass;
        blue += s.blue;
        red  += s.red;
        u  = (u * _mass + s.u * s.mass) * itotal_mass;
        //met  = (met * mass + s.met * s.mass) * itotal_mass;
    }

    return *this;
}


voxel operator*(const float c, const voxel& s)
{
    voxel temp;

    temp.mass = c * s.mass;
    temp.blue = c * s.blue;
    temp.red =  c * s.red;
    temp.u = s.u;
    //temp.met = s.met;

    return temp;
}


voxel operator*(const voxel& s, const float c)
{
    return c * s;
}


void voxel::zero()
{
    mass = 0;
    blue = 0;
    red = 0;
    u = 0;
    //met = 0;
}
