/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
 *  \brief Provides a class for storing a rectangular array of monochromatic
 *         pixels.
 */

#ifndef PIXEL_ARRAY_H
#define PIXEL_ARRAY_H

#include "Core/global.h"
#include "Core/particles.h"


/*! \brief A rectangular array of monochromatic pixels.

    Rectangular array of monochrome pixels. Each pixel contains a floating point
    value, corresponding to brightness, which can then be scaled for viewing on
    a computer monitor. In a sense, the array collects data like a virtual CCD.
    Once the array has collected data, post-processing can be performed to scale
    the brightness into a representable form. For instance, if one pixel is 1000
    times brighter than the average pixel, then a simple linear brightness scale
    is not useful for viewing the image; clipping would be necessary. This class
    can perform power law scaling (gamma correction) to compress the dynamical
    scale into a representable form while avoiding clipping. */
class pixel_array
{

protected:

    float* pixels {nullptr};
    kpc   view_size;
    float ipixel_size;
    float max_value;
    float min_value;
    float gamma;
    float aspect_ratio;
    pix   x_res;
    pix   y_res;
    pix   x_shift;
    pix   y_shift;
    uint  npixels;
    bool  normalized;

public:

    pixel_array(const pix _x_res, const pix _y_res, const kpc x_size);

    pixel_array();

    ~pixel_array();

    /*! Initialize the object */
    void init(const pix _x_res, const pix _y_res, const kpc x_size);

    /*! Free the allocated memory */
    void free();

    /*! Set all pixels in the image to fill_value */
    void fill(const float fill_value);

    /*! Adds a particle of a specified mass to the array at location (x, y)
        where x and y are coordinates measured in kiloparsecs. */
    void add_particle(const kpc x, const kpc y, const float mass);

    /*! Sets the value of the pixel at position (xi, yi).
     *  \param xi the horizontal coordinate of the pixel, measured from the left
     *  side of the array. \param yi the vertical coordinate of the pixel,
     *  measured from the top of the array. */
    void set_pixel(const pix xi, const pix yi, const float value);

    /*! increments the value of the pixel at (xi, yi). \param xi the horizontal
     *  coordinate of the pixel, measured from the left side of the array.
     *  \param yi the vertical coordinate of the pixel, measured from the top of
     *  the array. */
    void collect_photons(const pix xi, const pix yi, const float value);

    /*! Set the physical width of the image. \param x_dim The width of the image
        in units of kiloparsecs. */
    void set_view_size(const kpc x_dim);

    /*! Normalizes the image. The pixel with the largest intensity value is
     *  scaled to 1.0. This is usually followed by a gamma correction using the
     *  gamma_correction() method. */
    void normalize();

    /*!
     * \return true if the image is normalized, false otherwise.
     */
    bool is_normalized() const;

    /*! Sometimes the normalize() normalization method is not appropriate.
     *  Rather than dividing the entire image by its largest value, this method
     *  divides all pixels by white_in. Then, the pixels are multiplied by
     *  white_out. The image is then clipped such that pixels brighter than
     *  white_out are set to white_out and pixels dimmer than black_out are set
     *  to black_out. */
    void clip(const float black_out,
              const float white_in,
              const float white_out);

    /*! Automatically normalizes the image and then performs a
     *  gamma correction. If the image is already normalized, you should use the
     *  gamma_correction() function instead of this method. */
    void rescale_value(const float _gamma = 0.45);

    /*! Adjusts the scale of the pixel values so that the dynamic range can be
     *  displayed in an image file. \param _gamma an exponent that adjusts the
     *  scaling such that new_flux = old_flux^_gamma. \param _beta modifies the
     *  input before gamma correction is performed. Refer to the source for more
     *  details. */
    void gamma_correction(const float _gamma, const float _beta);

    /*! Retrieves the flux value at pixel coordinate (xi, yi) */
    float get_pixel(const pix xi, const pix yi) const;

private:

    /*! particle_type_group::rect_mesh is a friend function in order to
     *  facilitate communication with pixel_array objects. The friend attribute
     *  allows us to perform optimizations that are otherwise more difficult.
     *  The rect_mesh function is the most severe bottleneck in the
     *  interactive preview routine, so it needs all the help that it can get.*/
    friend void particle_type_group::rect_mesh(pixel_array* canvas);
};

#endif
