/* GSnap -- a program for analyzing, visualizing, and manipulating
snapshots from galaxy simulations.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
    \brief Contains global configuration options.

     Contains global compile-time configuration options. This should be included
     in all of GSnap's header files!

     \todo
     Make some of the quantities defined here into user inputs rather than
     compile-time constants.
*/

#ifndef SETTINGS_H
    #define SETTINGS_H

    #define VERSION "0.8.5, September 2013"

    // default parameter values

    // the number of projections of the system used for computing statistics
    #define NP 1000

    // the size of the viewing area for the --view option
    #define VIEW_SIZE 125

    // the width and height of the output image in pixels.
    #define IMAGE_WIDTH 600
    #define IMAGE_HEIGHT 600

    // the width and length of the slit used by the command line interface sigma
    // calculation routines.

    #define SLIT_WIDTH 1
    #define SLIT_HEIGHT 5

    /********************** basic volume rendering settings *******************/

    // the minimum number of overlapping SPH particles
    #define N_MIN_GAS 16

    // the maximum number of overlapping SPH particles
    #define N_MAX_GAS 18

    // the opacity of the gas when performing volume rendering
    #define OPACITY 30

    /**************************************************************************/


    /**** Settings related to the pre-processing stage of volume rendering ****/

    // the number of voxel widths per coarse voxel width. This should either be
    // a user-definable parameter or an automatically computed quantity.
    #define COARSE_FINE 10

    // begin nearest neighbor search in a region of this radius [cvoxel_widths]
    #define INITIAL_SEARCH_RADIUS 2

    // end nearest neighbor search when the search radius reaches this value
    // [cvoxel_widths]
    #define MAX_SEARCH_RADIUS 4

    // Simply including N_MIN_GAS particles in the list of nearest neighbor
    // candidates leads to the possibility of quite poor results. Thus, at least
    // OVER_MIN_FACTOR * N_MIN_GAS particles will be included in the list.
    #define OVER_MIN_FACTOR 3

    /**************************************************************************/

    // parameters for scaling the brightness of pixels.

    #define GAS_GAMMA 0.65
    #define GAS_BETA 0.2
    #define GAS_CLIP_MIN  0
    #define GAS_CLIP_MAX_VAL 0.03
    #define GAS_CLIP_MAX_NORM 1

    #define STAR_GAMMA 0.66
    #define STAR_BETA 3
    #define STAR_CLIP_MIN 0
    #define STAR_CLIP_MAX_VAL 4.5
    #define STAR_CLIP_MAX_NORM 1.0

#endif
