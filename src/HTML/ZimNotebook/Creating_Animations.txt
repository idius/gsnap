Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2012-08-05T12:38:01-07:00

====== Creating Animations ======

Creating animations is a fairly trivial task, since many utilities are able to convert a series of still images into a video. The more difficult task is creating the //frames// for videos. In this section you will learn how to perform both tasks. You will also learn to perform batch mode post-processing and the how to create frames for 3D animations.

[[Creating Animations:Creating Frames|Creating frames for an animation]]
[[Creating Animations:Creating a video from still frames|Creating a video from a series of frames]]
[[Creating Animations:Post-Processing Images|Post-processing images]]
[[Creating Animations:Creating 3D frames|Creating frames for 3D movies]]



