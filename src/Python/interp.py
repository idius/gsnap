"""
This script renames the snapshot files to allow space for interpolated 
snapshots. It then calls GSnap for each set of snapshot files and 
names the interpolated snapshot files such that they are interleaved 
with the original snapshot files.
"""
import glob
import shutil
import sys
from subprocess import call


files=glob.glob('*snapshot_*')
files.sort()

for i in range(1, files.__len__()):
    file0=files[i-1]
    file1=files[i]
    interp_file=file0
    command=["gsnap", "-i","4", file0, file1, interp_file]
    call(command)
    
#rename the files    
    
files=glob.glob('*snapshot_*')
files.sort()    
    
count=0;
for current_file in files:
    shutil.move(current_file, current_file.rstrip('.0123456789')+'{0:05d}'.format(count));
    count=count+1;
