"""
Automatically computes the directional velocity dispersion statistics on a
series of snapshot files using GSnap. In order to improve performance on
machines with multiple processors, the snapshots are processed in parallel.
The script assumes the following:

    * the snapshot file names contain "snapshot_".

    * the current working directory contains the snapshots of interest.

    * the gsnap executable is located in the directory containing the snapshots.

    * you are using python version >=2.7 (the subprocess.check_output was
      introduced in version 2.7).

    * joblib is installed. Get it here: http://packages.python.org/joblib/

If these requirements are met, simply run 'python auto_gnsap.py' and wait for
the computation to finish. The output data can be found in sig_time_series.txt.
"""

import glob
from subprocess import check_output
from joblib import Parallel, delayed

def compute_stats(snapshot):
    command = ["gsnap", "-nt", "001111", "-w", "1", "-l", "6", snapshot]
    return check_output(command)


snapshot_search_string = '*snapshot_*'

output_file_name = 'sig_time_series.txt'

files = glob.glob(snapshot_search_string)

output = Parallel(n_jobs=-1)(delayed(compute_stats)(s) for s in files)

# We do not bother to sort the output chronologically here. The sorting
# can be done by the plotting program.

f = open(output_file_name, 'w')

for i in range(files.__len__()):
    f.write(output[i])

f.close()
