import sys
import numpy


def to_float(n_string_list):
    return [float(n_string) for n_string in n_string_list]


def read_ascii(file_name):
    """
    Loads a data table from an ASCII file. Converts the numbers to
    floating point values and sorts the entries by the data in the
    first column. Then stores the data in a two dimensional numpy
    array. The return value is a 2-dimensional numpy array in the
    format: (row numer, column number).
    """
    f = open(file_name, 'r')

    table = [line.split('\t') for line in f]

    # convert the text list entries to floating point values

    f_table = [to_float(line) for line in table]

    # by default, this sorts the table by the first entry in each row
    f_table.sort()

    return numpy.array(f_table)

    f.close()
