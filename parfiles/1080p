# A sample parameter file

# velocity dispersion settings
SlitWidth   2
SlitLength  8
Projections 1000 # the number of projections used when calculating statistics

# general image creation properties
ImageWidth    1920 #1280 # measured in pixels
ImageHeight   1080 #720 # measured in pixels
ViewWidth     110  # measured in kiloparsecs
ViewDepth     50  # measured in kiloparsecs

# SPH volume rendering settings
MinNeighbors   28       # minimum number of SPH particles included in the sum
MeanNeighbors  29       # desired mean number of SPH particles included in sum
MaxNeighbors   30       # maximum number of SPH particles included in the sum
GasOpacity     20       # opacity of the gas for the volume rendering
GasGamma       0.7
GasClipMin     5e-5     # pixels dimmer than this will be clipped. This is the
                        # value of the dimmest pixels in the pixel array.

GasClipMaxVal  0.04     # pixels brighter than this will be clipped.
GasClipMaxNorm 1.0      # The brightest pixels in the final image will be set to
                        # this fraction of white. For instance a value of 0.95
                        # means that the largest sub-pixel values in the image
                        # will be 255*0.95 = 242 (i.e., {R,G,B} = {242,242,242})

# Star rendering settings
StarGamma        0.47
StarClipMin      0
StarClipMaxVal   4.6
StarClipMaxNorm  1.0     # these have the same meaning as the "Clip" quantities
                         # above
