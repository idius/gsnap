# A sample parameter file

# velocity dispersion settings
SlitWidth   2
SlitLength  8
Projections 1000 # the number of projections used when calculating statistics

# general image creation properties
ImageWidth    800 # measured in pixels
ImageHeight   800 # measured in pixels
ViewWidth     50  # measured in kiloparsecs
ViewDepth     20  # measured in kiloparsecs

# SPH volume rendering settings
MinNeighbors   35       # minimum number of SPH particles included in the sum
MeanNeighbors  38       # desired mean number of SPH particles included in sum
MaxNeighbors   42       # maximum number of SPH particles included in the sum
GasOpacity     25       # opacity of the gas for the volume rendering
GasGamma       0.7
GasClipMin     5e-5     # pixels dimmer than this will be clipped. This is the
                        # value of the dimmest pixels in the pixel array.

GasClipMaxVal  0.06     # pixels brighter than this will be clipped.
GasClipMaxNorm 1.0      # The brightest pixels in the final image will be set to
                        # this fraction of white. For instance a value of 0.95
                        # means that the largest sub-pixel values in the image
                        # will be 255*0.95 = 242 (i.e., {R,G,B} = {242,242,242})

# Star rendering settings
StarGamma        0.49
StarClipMin      0
StarClipMaxVal   3.7
StarClipMaxNorm  1.0     # these have the same meaning as the "Clip" quantities
                         # above
